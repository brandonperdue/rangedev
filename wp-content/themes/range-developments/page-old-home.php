<?php defined( 'ABSPATH' ) || exit; ?><!DOCTYPE html><!--  Last Published: Tue Nov 20 2018 19:51:23 GMT+0000 (UTC)  --><html data-wf-page="5bc09cd31aa58d8863a2bdae" data-wf-site="5ab00568c82e2061c2a05a58"><head>
  <meta charset="utf-8">
  
  
  
  
  
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>css/normalize.css?v=1542837575" rel="stylesheet" type="text/css">
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>css/components.css?v=1542837575" rel="stylesheet" type="text/css">
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>css/range-developments.css?v=1542837575" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Roboto:regular,700"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>images/favicon.png?v=1542837575" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>images/webclip.png?v=1542837575" rel="apple-touch-icon">
  <meta name="ahrefs-site-verification" content="714c4b625dd53181fcfed919e97e4e4ae294cc8fb251d1cb911b02256a21e297">
  <meta name="google-site-verification" content="7_G9HvmS0Ee9hApkUO-UoLrk6eJx75oDWDroa2R4otE">
  <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
  <!--  Global site tag (gtag.js) - Google Analytics  -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-4118182-47"></script>
  <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-4118182-47');
</script>
  <script src="https://global.localizecdn.com/localize.js"></script>
  <script>!function(a){if(!a.Localize){a.Localize={};for(var e=["translate","untranslate","phrase","initialize","translatePage","setLanguage","getLanguage","detectLanguage","getAvailableLanguages","untranslatePage","bootstrap","prefetch","on","off"],t=0;t<e.length;t++)a.Localize[e[t]]=function(){}}}(window);</script>
  <script>
 Localize.initialize({
  key: 'reyBHeN3Gieh5',
  rememberLanguage: true
 });
</script>
  <script>
window.onload = function(){
 if (Localize.getLanguage() === "ar") {
   document.getElementsByTagName("body")[0].setAttribute("dir", "rtl");
   document.getElementById("Hero").setAttribute("dir", "ltr");
   if (document.getElementById("slide1")) {document.getElementById("slide1").setAttribute("dir", "rtl");}
   if (document.getElementById("slide2")) {document.getElementById("slide2").setAttribute("dir", "rtl");}
   if (document.getElementById("hero-call-text")) {document.getElementById("hero-call-text").setAttribute("dir", "rtl");}
   if (document.getElementById("phone-number-dir")) {document.getElementById("phone-number-dir").setAttribute("dir", "ltr");}
   if (document.getElementById("Testimonials")) {document.getElementById("Testimonials").setAttribute("dir", "ltr");}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").setAttribute("dir", "rtl");}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").setAttribute("dir", "rtl");}
   document.getElementsByTagName("body")[0].style.textAlign = "right";
   document.getElementById("Hero").style.textAlign = "right";
   if (document.getElementById("slide1")) {document.getElementById("slide1").style.textAlign = "right";}
   if (document.getElementById("slide2")) {document.getElementById("slide2").style.textAlign = "right";}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").style.textAlign = "right";}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").style.textAlign = "right";}
   if ($('.vertical-header-wrapper')) {$('.vertical-header-wrapper').css('right', '0'); $('.vertical-header').css('left', '124px');}
   if (document.getElementById("wistia-bqquvi10jm-1_popover")) {document.getElementById("wistia-bqquvi10jm-1_popover").style.right = "-99999px";document.getElementById("wistia-bqquvi10jm-1_popover").style.left = "auto";}
   if (document.getElementById("wistia-bqquvi10jm-1_popover_popover_close_button")) {document.getElementById("wistia-bqquvi10jm-1_popover_popover_close_button").style.right = "-99999px";document.getElementById("wistia-bqquvi10jm-1_popover").style.left = "auto";}
 }
}
Localize.on("setLanguage", function(data) {
 if (data.to === "ar") {
   document.getElementsByTagName("body")[0].setAttribute("dir", "rtl");
   document.getElementById("Hero").setAttribute("dir", "ltr");
   if (document.getElementById("slide1")) {document.getElementById("slide1").setAttribute("dir", "rtl");}
   if (document.getElementById("slide2")) {document.getElementById("slide2").setAttribute("dir", "rtl");}
   if (document.getElementById("hero-call-text")) {document.getElementById("hero-call-text").setAttribute("dir", "rtl");}
   if (document.getElementById("phone-number-dir")) {document.getElementById("phone-number-dir").setAttribute("dir", "ltr");}
   if (document.getElementById("Testimonials")) {document.getElementById("Testimonials").setAttribute("dir", "ltr");}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").setAttribute("dir", "rtl");}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").setAttribute("dir", "rtl");}
   document.getElementsByTagName("body")[0].style.textAlign = "right";
   document.getElementById("Hero").style.textAlign = "right";
   if (document.getElementById("slide1")) {document.getElementById("slide1").style.textAlign = "right";}
   if (document.getElementById("slide2")) {document.getElementById("slide2").style.textAlign = "right";}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").style.textAlign = "right";}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").style.textAlign = "right";}
 }
 if (data.to != "ar") {
   document.getElementsByTagName("body")[0].setAttribute("dir", "ltr");
   if (document.getElementById("slide1")) {document.getElementById("slide1").setAttribute("dir", "ltr");}
   if (document.getElementById("slide2")) {document.getElementById("slide2").setAttribute("dir", "ltr");}
   if (document.getElementById("hero-call-text")) {document.getElementById("hero-call-text").setAttribute("dir", "ltr");}
   if (document.getElementById("phone-number-dir")) {document.getElementById("phone-number-dir").setAttribute("dir", "ltr");}
   if (document.getElementById("Testimonials")) {document.getElementById("Testimonials").setAttribute("dir", "ltr");}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").setAttribute("dir", "ltr");}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").setAttribute("dir", "ltr");}
   document.getElementsByTagName("body")[0].style.textAlign = "left";
   document.getElementById("Hero").style.textAlign = "left";
   if (document.getElementById("slide1")) {document.getElementById("slide1").style.textAlign = "left";}
   if (document.getElementById("slide2")) {document.getElementById("slide2").style.textAlign = "left";}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").style.textAlign = "left";}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").style.textAlign = "left";}
 }
})
</script>
<?php wp_enqueue_script("jquery"); wp_head(); ?></head>
<body class="body <?php echo " ".join( ' ', get_body_class() ); ?>" udesly-page-name="old-home"><?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div data-ix="preloader" class="preloader"></div>
  <div data-ix="page-load" class="page-wrapper">
    <div class="call-back">
      <div class="call-back-wrapper">
        <div class="call-back-popup"><a href="#" data-ix="hide-call-back-popup" class="close-popup-button w-inline-block"></a>
          <div class="contact-form-wrapper w-form">
            <form id="email-form" name="email-form" data-name="Email Form" class="contact-form">
              <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-113239269"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_-113239269','text'); ?></udesly-fe><label for="name-2" class="form-label">Name</label><input type="text" class="input w-input" maxlength="256" name="name-2" data-name="Name 2" placeholder="Enter your name" id="name-2" required><label for="email-2" class="form-label">Email</label><input type="text" class="input w-input" maxlength="256" name="email-2" data-name="Email 2" placeholder="Enter your email" id="email-2" required><label for="Phone-3" class="form-label">Phone</label><input type="text" class="input w-input" maxlength="256" name="Phone-3" data-name="Phone 3" placeholder="Enter your phone" id="Phone-3" required><label for="Country-2" class="form-label">Country</label><input type="text" class="input w-input" maxlength="256" name="Country-2" data-name="Country 2" placeholder="Enter your country" id="Country-2" required><label for="Message-2" class="form-label">Message</label><textarea id="Message-2" name="Message-2" placeholder="How can we help you?" maxlength="5000" data-name="Message 2" required class="input text-area w-input"></textarea><input type="submit" value="Contact Us" data-wait="Please wait..." class="button form-button w-button"></form>
            <div class="w-form-done">
              <div>Thank you! Your submission has been received!</div>
            </div>
            <div class="w-form-fail">
              <div>Oops! Something went wrong while submitting the form.</div>
            </div>
          </div>
        </div>
        <div data-ix="hide-call-back-popup" class="popup-overlay"></div>
      </div>
      <a href="#" data-ix="show-call-back-popup" class="call-back-button w-inline-block">
        <div class="contact-popup">Contact Us</div>
      </a>
    </div>
    <div data-collapse="medium" data-animation="default" data-duration="400" class="nav-bar fixed-nav-bar w-nav">
      <div class="wrapper w-container"><a href="<?php echo get_site_url(); ?>" class="logo w-nav-brand"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-719241259"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-719241259','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-719241259','image-alt'); ?>" class="image-8" srcset="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-719241259','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a>
        <nav role="navigation" class="nav-menu pop-up w-nav-menu">
          <div data-delay="0" data-hover="1" class="w-dropdown">
            <div class="nav-link pop-up w-dropdown-toggle">
              <div class="dropdown-icon w-icon-dropdown-toggle"></div>
              <div>Properties</div>
            </div>
            <nav class="dropdown w-dropdown-list"><a href="<?php echo udesly_get_permalink_by_slug('park-hyatt-st-kitts'); ?>" class="dropdown-link w-dropdown-link">Park Hyatt St. Kitts</a><a href="<?php echo udesly_get_permalink_by_slug('cabrits-resort-kempinski'); ?>" class="dropdown-link w-dropdown-link">Cabrits Resort Kempinski</a><a href="<?php echo udesly_get_permalink_by_slug('six-senses-st-kitts'); ?>" class="dropdown-link w-dropdown-link">Six Senses St. Kitts</a></nav>
          </div><a href="<?php echo udesly_get_permalink_by_slug('about-range-developments'); ?>" class="nav-link pop-up w-nav-link">About Us</a><a href="<?php echo udesly_get_permalink_by_slug('media-coverage'); ?>" class="nav-link pop-up w-nav-link">Media</a><a href="<?php echo udesly_get_permalink_by_slug('blog'); ?>" class="nav-link pop-up w-nav-link">Blog</a><a href="<?php echo udesly_get_permalink_by_slug('citizenship-by-investment'); ?>" class="button fixed-nav-bar-button w-button">Invest in Citizenship</a></nav>
        <div class="menu-button fixed-menu-button w-nav-button">
          <div class="w-icon-nav-menu"></div>
        </div>
      </div>
    </div>
    <div id="Hero" data-ix="show-nav-bar-on-scroll" class="image-section hero-section">
      <div data-collapse="medium" data-animation="default" data-duration="400" class="nav-bar w-nav">
        <div class="wrapper w-container"><a href="<?php echo get_site_url(); ?>" class="logo w-nav-brand"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_272980421"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_272980421','image-src'); ?>" width="200" height="63" alt="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_272980421','image-alt'); ?>" class="image-4" srcset="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_272980421','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a>
          <nav role="navigation" class="nav-menu w-nav-menu">
            <div data-delay="0" data-hover="1" class="w-dropdown">
              <div class="nav-link w-dropdown-toggle">
                <div class="dropdown-icon w-icon-dropdown-toggle"></div>
                <div>Properties</div>
              </div>
              <nav class="dropdown w-dropdown-list"><a href="<?php echo udesly_get_permalink_by_slug('park-hyatt-st-kitts'); ?>" class="dropdown-link w-dropdown-link">Park Hyatt St. Kitts</a><a href="<?php echo udesly_get_permalink_by_slug('cabrits-resort-kempinski'); ?>" class="dropdown-link w-dropdown-link">Cabrits Resort Kempinski</a><a href="<?php echo udesly_get_permalink_by_slug('six-senses-st-kitts'); ?>" class="dropdown-link w-dropdown-link">Six Senses St. Kitts</a></nav>
            </div><a href="<?php echo udesly_get_permalink_by_slug('about-range-developments'); ?>" class="nav-link w-nav-link">About Us</a><a href="<?php echo udesly_get_permalink_by_slug('media-coverage'); ?>" class="nav-link w-nav-link">Media</a><a href="<?php echo udesly_get_permalink_by_slug('blog'); ?>" class="nav-link w-nav-link">Blog</a><a href="<?php echo udesly_get_permalink_by_slug('citizenship-by-investment'); ?>" class="button white-button nav-bar-button w-button">Invest in Citizenship</a></nav>
          <div class="menu-button w-nav-button">
            <div class="w-icon-nav-menu"></div>
          </div>
        </div>
      </div>
      <div class="hero-call-text-wrapper">
        <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1577052701"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_-1577052701','text'); ?></udesly-fe>
      </div>
      <div data-delay="8000" data-animation="slide" data-autoplay="1" data-duration="600" data-infinite="1" class="hero-slider w-slider">
        <div class="w-slider-mask">
          <div class="slide-1 w-slide">
            <div class="wrapper slide-wrapper w-container">
              <div id="slide1" class="slide-content">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-542585592"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_-542585592','text'); ?></udesly-fe>
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_966592990"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_966592990','text'); ?></udesly-fe><a data-ix="fade-up-4" href="<?php echo udesly_get_permalink_by_slug('about-range-developments'); ?>" class="button hero-button w-button">Learn More</a>
                <div class="button ghost-white-button video w-embed w-script">
                  <script src="https://fast.wistia.com/embed/medias/bqquvi10jm.jsonp" async></script>
                  <script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_bqquvi10jm popover=true popoverContent=link" style="display:inline;position:relative"><a href="#" style="color:white; text-decoration:none;">Watch Video</a></span></div>
              </div>
            </div>
          </div>
          <div class="slide-2 w-slide">
            <div class="wrapper slide-wrapper w-container">
              <div id="slide2" class="slide-content">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1163305115"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_1163305115','text'); ?></udesly-fe>
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1047016032"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_1047016032','text'); ?></udesly-fe><a href="<?php echo udesly_get_permalink_by_slug('citizenship-by-investment'); ?>" class="button hero-button w-button">Learn more about CBI</a></div>
            </div>
          </div>
        </div>
        <div class="slider-arrow arrow-left w-hidden-small w-hidden-tiny w-slider-arrow-left"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-265916240"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-265916240','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-265916240','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-265916240','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></div>
        <div class="slider-arrow arrow-right w-hidden-small w-hidden-tiny w-slider-arrow-right"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_550220069"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_550220069','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_550220069','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_550220069','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></div>
        <div class="w-hidden-main w-hidden-medium w-hidden-small w-hidden-tiny w-slider-nav w-round"></div>
      </div>
      <div class="hero-bottom">
        <div class="wrapper hero-bottom-wrapper w-container">
          <div class="hero-social">
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-436807571"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_-436807571','text'); ?></udesly-fe>
            <div class="hero-social-line"></div>
            <div class="social-icons"><a href="https://www.instagram.com/range_developments/" target="_blank" class="social-link w-inline-block"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-1288260099"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-1288260099','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-1288260099','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-1288260099','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a><a href="https://twitter.com/RangeDevelop?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank" class="social-link w-inline-block"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-790717732"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-790717732','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-790717732','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-790717732','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a><a href="https://www.facebook.com/rangedevelopments/" target="_blank" class="social-link w-inline-block"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_446961179"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_446961179','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_446961179','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_446961179','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a><a href="https://www.linkedin.com/company/range-developments/" target="_blank" class="social-link w-inline-block"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_1398343091"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_1398343091','image-src'); ?>" width="16" height="16" alt="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_1398343091','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_1398343091','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a></div>
          </div><a href="#Features" data-ix="scroll-down-button-hover" class="scroll-down-button w-inline-block"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-227153291"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-227153291','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-227153291','image-alt'); ?>" class="scroll-arrow-icon" srcset="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-227153291','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a></div>
      </div>
    </div>
    <div id="Features" class="section">
      <div class="vertical-header-wrapper">
        <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1509338190"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_1509338190','text'); ?></udesly-fe>
      </div>
      <div class="wrapper w-container">
        <div class="section-header-wrapper">
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1705357658"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_-1705357658','text'); ?></udesly-fe>
        </div>
        <div class="feature-cards">
          <div data-ix="fade-up-1" class="feature-card">
            <div class="card-corner">
              <div class="card-corner-line"></div>
            </div>
            <div class="hotel-logo-container"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_1444775660"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_1444775660','image-src'); ?>" width="56" alt="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_1444775660','image-alt'); ?>" class="feature-icon" srcset="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_1444775660','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></div>
            <div class="hotel-blurb">
              <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-872032947"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_-872032947','text'); ?></udesly-fe>
              <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_284450351"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_284450351','text'); ?></udesly-fe><a href="<?php echo udesly_get_permalink_by_slug('park-hyatt-st-kitts'); ?>" class="feature-link white">Learn More</a></div>
          </div>
          <div data-ix="fade-up-2" class="feature-card cabrits">
            <div class="card-corner">
              <div class="card-corner-line"></div>
            </div>
            <div class="hotel-logo-container"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-561218721"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-561218721','image-src'); ?>" width="56" alt="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-561218721','image-alt'); ?>" class="feature-icon vertical" srcset="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-561218721','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></div>
            <div class="hotel-blurb">
              <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_758101903"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_758101903','text'); ?></udesly-fe>
              <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-326121056"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_-326121056','text'); ?></udesly-fe><a href="<?php echo udesly_get_permalink_by_slug('cabrits-resort-kempinski'); ?>" class="feature-link white">Learn More</a></div>
          </div>
          <div data-ix="fade-up-3" class="feature-card six-senses">
            <div class="card-corner">
              <div class="card-corner-line"></div>
            </div>
            <div class="hotel-logo-container"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_130574976"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_130574976','image-src'); ?>" width="56" srcset="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_130574976','image-srcset'); ?>" sizes="(max-width: 479px) 100vw, (max-width: 767px) 200px, (max-width: 991px) 25vw, 17vw" alt="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_130574976','image-alt'); ?>" class="feature-icon"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></div>
            <div class="hotel-blurb">
              <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1395187036"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_-1395187036','text'); ?></udesly-fe>
              <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1752975592"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_1752975592','text'); ?></udesly-fe><a href="<?php echo udesly_get_permalink_by_slug('six-senses-st-kitts'); ?>" class="feature-link white">Learn More</a></div>
          </div>
        </div>
      </div>
    </div>
    <div id="Testimonials" class="image-section testimonials-section">
      <div data-animation="slide" data-duration="600" data-infinite="1" class="testimonials w-slider">
        <div class="w-slider-mask">
          <div id="test-slide-1" class="w-slide">
            <div class="review-slide"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_118027746"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_118027746','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_118027746','image-alt'); ?>" class="review-avatar" srcset="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_118027746','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe>
              <div class="review-content">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1380442496"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_1380442496','text'); ?></udesly-fe>
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_421927120"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_421927120','text'); ?></udesly-fe>
                <div class="review-author-info">// Prime Minister of Dominica</div>
              </div>
            </div>
          </div>
          <div id="test-slide-2" class="w-slide">
            <div class="review-slide"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-1951273038"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-1951273038','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-1951273038','image-alt'); ?>" class="review-avatar" srcset="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-1951273038','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe>
              <div class="review-content">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1606380084"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_1606380084','text'); ?></udesly-fe>
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1391193530"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_1391193530','text'); ?></udesly-fe>
                <div class="review-author-info">// Prime Minister of St. Kitts and Nevis</div>
              </div>
            </div>
          </div>
        </div>
        <div class="slider-arrow arrow-left w-slider-arrow-left"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-265916240"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-265916240','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-265916240','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-265916240','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></div>
        <div class="slider-arrow arrow-right w-slider-arrow-right"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_550220069"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_550220069','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_550220069','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_550220069','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></div>
        <div class="testimonoals-nav w-hidden-main w-hidden-medium w-slider-nav w-round"></div>
      </div>
    </div>
    <div class="section">
      <div class="vertical-header-wrapper">
        <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-428769103"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_-428769103','text'); ?></udesly-fe>
      </div>
      <div class="wrapper w-container">
        <div class="side-feature">
          <div class="side-feature-image"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_1680451695"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_1680451695','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_1680451695','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_1680451695','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></div>
          <div class="side-feature-content">
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-417028567"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_-417028567','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1923152604"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_-1923152604','text'); ?></udesly-fe>
            <div class="side-feature-mini-wrapper">
              <div class="side-feature-mini">
                <div class="side-feature-mini-icon w-embed"><i class="fas fa-lock fa-3x"></i></div>
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1006646037"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_1006646037','text'); ?></udesly-fe>
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_574190864"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_574190864','text'); ?></udesly-fe>
              </div>
              <div class="side-feature-mini">
                <div class="side-feature-mini-icon w-embed"><i class="far fa-check-circle fa-3x"></i></div>
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1291612529"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_-1291612529','text'); ?></udesly-fe>
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1809497405"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_-1809497405','text'); ?></udesly-fe>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="section facts-section">
      <div class="wrapper w-container">
        <div class="facts">
          <div data-ix="fade-up-1" class="fact-item"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-998480259"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-998480259','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-998480259','image-alt'); ?>" class="statistic-icon" srcset="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_-998480259','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-15007055"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_-15007055','text'); ?></udesly-fe>
          </div>
          <div data-ix="fade-up-2" class="fact-item"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_1400565105"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_1400565105','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_1400565105','image-alt'); ?>" class="statistic-icon" srcset="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_1400565105','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1774257024"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_1774257024','text'); ?></udesly-fe>
          </div>
          <div data-ix="fade-up-3" class="fact-item"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_939094943"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_939094943','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_939094943','image-alt'); ?>" class="statistic-icon" srcset="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_939094943','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_408321941"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_408321941','text'); ?></udesly-fe>
          </div>
          <div data-ix="fade-up-4" class="fact-item"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_212724350"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_212724350','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_212724350','image-alt'); ?>" class="statistic-icon" srcset="<?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_image_212724350','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_530491223"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_530491223','text'); ?></udesly-fe>
          </div>
        </div>
      </div>
    </div>
    <div class="section no-side-paddings">
      <div class="vertical-header-wrapper">
        <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1939703499"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_-1939703499','text'); ?></udesly-fe>
      </div>
      <div class="big-side-feature">
        <div class="big-side-feature-content">
          <div class="big-side-feature-info">
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_2018332020"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_2018332020','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-333171991"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_-333171991','text'); ?></udesly-fe>
          </div>
        </div>
        <div class="big-side-feature-image image-1"></div>
      </div>
    </div>
    <div class="section instagram-section">
      <div class="instagram">
        <div class="instagram-photos">
          <a data-ix="instagram-photo" href="#" target="_blank" class="instagram-photo photo-1 w-inline-block">
            <div class="photo-fade"></div>
          </a>
          <a data-ix="instagram-photo" href="#" target="_blank" class="instagram-photo photo-2 w-inline-block">
            <div class="photo-hover"></div>
          </a><a data-ix="instagram-photo" href="#" target="_blank" class="instagram-photo photo-3 w-inline-block"></a><a data-ix="instagram-photo" href="#" target="_blank" class="instagram-photo photo-5 w-inline-block"></a><a data-ix="instagram-photo" href="#" class="instagram-photo photo-4 w-inline-block"></a></div>
        <div class="instagram-info-wrapper">
          <div class="instagram-info">
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_202938515"><?php echo udesly_get_frontend_editor_content('old-home_udesly_frontend_editor_text_202938515','text'); ?></udesly-fe><a target="_blank" href="<?php echo udesly_get_permalink_by_slug('about-range-developments'); ?>" class="button instagram-button w-button">Feel Good</a></div>
        </div>
      </div>
    </div>
	<?php get_footer(); ?>
<?php wp_footer(); ?><?php endwhile; endif; ?></body></html>