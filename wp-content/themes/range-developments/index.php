<?php defined( 'ABSPATH' ) || exit; ?><!DOCTYPE html><!--  Last Published: Tue Nov 20 2018 19:51:23 GMT+0000 (UTC)  --><html data-wf-page="5b4f4f6a6c24e544b3e09e1a" data-wf-site="5ab00568c82e2061c2a05a58"><head>
  <meta charset="utf-8">
  
  
  
  
  
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>css/normalize.css?v=1542837569" rel="stylesheet" type="text/css">
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>css/components.css?v=1542837569" rel="stylesheet" type="text/css">
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>css/range-developments.css?v=1542837569" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Roboto:regular,700"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>images/favicon.png?v=1542837569" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>images/webclip.png?v=1542837569" rel="apple-touch-icon">
  <meta name="ahrefs-site-verification" content="714c4b625dd53181fcfed919e97e4e4ae294cc8fb251d1cb911b02256a21e297">
  <meta name="google-site-verification" content="7_G9HvmS0Ee9hApkUO-UoLrk6eJx75oDWDroa2R4otE">
  <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
  <!--  Global site tag (gtag.js) - Google Analytics  -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-4118182-47"></script>
  <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-4118182-47');
</script>
  <script src="https://global.localizecdn.com/localize.js"></script>
  <script>!function(a){if(!a.Localize){a.Localize={};for(var e=["translate","untranslate","phrase","initialize","translatePage","setLanguage","getLanguage","detectLanguage","getAvailableLanguages","untranslatePage","bootstrap","prefetch","on","off"],t=0;t<e.length;t++)a.Localize[e[t]]=function(){}}}(window);</script>
  <script>
 Localize.initialize({
  key: 'reyBHeN3Gieh5',
  rememberLanguage: true
 });
</script>
  <script>
window.onload = function(){
 if (Localize.getLanguage() === "ar") {
   document.getElementsByTagName("body")[0].setAttribute("dir", "rtl");
   document.getElementById("Hero").setAttribute("dir", "ltr");
   if (document.getElementById("slide1")) {document.getElementById("slide1").setAttribute("dir", "rtl");}
   if (document.getElementById("slide2")) {document.getElementById("slide2").setAttribute("dir", "rtl");}
   if (document.getElementById("hero-call-text")) {document.getElementById("hero-call-text").setAttribute("dir", "rtl");}
   if (document.getElementById("phone-number-dir")) {document.getElementById("phone-number-dir").setAttribute("dir", "ltr");}
   if (document.getElementById("Testimonials")) {document.getElementById("Testimonials").setAttribute("dir", "ltr");}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").setAttribute("dir", "rtl");}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").setAttribute("dir", "rtl");}
   document.getElementsByTagName("body")[0].style.textAlign = "right";
   document.getElementById("Hero").style.textAlign = "right";
   if (document.getElementById("slide1")) {document.getElementById("slide1").style.textAlign = "right";}
   if (document.getElementById("slide2")) {document.getElementById("slide2").style.textAlign = "right";}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").style.textAlign = "right";}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").style.textAlign = "right";}
   if ($('.vertical-header-wrapper')) {$('.vertical-header-wrapper').css('right', '0'); $('.vertical-header').css('left', '124px');}
   if (document.getElementById("wistia-bqquvi10jm-1_popover")) {document.getElementById("wistia-bqquvi10jm-1_popover").style.right = "-99999px";document.getElementById("wistia-bqquvi10jm-1_popover").style.left = "auto";}
   if (document.getElementById("wistia-bqquvi10jm-1_popover_popover_close_button")) {document.getElementById("wistia-bqquvi10jm-1_popover_popover_close_button").style.right = "-99999px";document.getElementById("wistia-bqquvi10jm-1_popover").style.left = "auto";}
 }
}
Localize.on("setLanguage", function(data) {
 if (data.to === "ar") {
   document.getElementsByTagName("body")[0].setAttribute("dir", "rtl");
   document.getElementById("Hero").setAttribute("dir", "ltr");
   if (document.getElementById("slide1")) {document.getElementById("slide1").setAttribute("dir", "rtl");}
   if (document.getElementById("slide2")) {document.getElementById("slide2").setAttribute("dir", "rtl");}
   if (document.getElementById("hero-call-text")) {document.getElementById("hero-call-text").setAttribute("dir", "rtl");}
   if (document.getElementById("phone-number-dir")) {document.getElementById("phone-number-dir").setAttribute("dir", "ltr");}
   if (document.getElementById("Testimonials")) {document.getElementById("Testimonials").setAttribute("dir", "ltr");}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").setAttribute("dir", "rtl");}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").setAttribute("dir", "rtl");}
   document.getElementsByTagName("body")[0].style.textAlign = "right";
   document.getElementById("Hero").style.textAlign = "right";
   if (document.getElementById("slide1")) {document.getElementById("slide1").style.textAlign = "right";}
   if (document.getElementById("slide2")) {document.getElementById("slide2").style.textAlign = "right";}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").style.textAlign = "right";}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").style.textAlign = "right";}
 }
 if (data.to != "ar") {
   document.getElementsByTagName("body")[0].setAttribute("dir", "ltr");
   if (document.getElementById("slide1")) {document.getElementById("slide1").setAttribute("dir", "ltr");}
   if (document.getElementById("slide2")) {document.getElementById("slide2").setAttribute("dir", "ltr");}
   if (document.getElementById("hero-call-text")) {document.getElementById("hero-call-text").setAttribute("dir", "ltr");}
   if (document.getElementById("phone-number-dir")) {document.getElementById("phone-number-dir").setAttribute("dir", "ltr");}
   if (document.getElementById("Testimonials")) {document.getElementById("Testimonials").setAttribute("dir", "ltr");}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").setAttribute("dir", "ltr");}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").setAttribute("dir", "ltr");}
   document.getElementsByTagName("body")[0].style.textAlign = "left";
   document.getElementById("Hero").style.textAlign = "left";
   if (document.getElementById("slide1")) {document.getElementById("slide1").style.textAlign = "left";}
   if (document.getElementById("slide2")) {document.getElementById("slide2").style.textAlign = "left";}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").style.textAlign = "left";}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").style.textAlign = "left";}
 }
})
</script>
<?php wp_enqueue_script("jquery"); wp_head(); ?></head>
<body class="body " udesly-page-name="blog">
  <div data-ix="preloader" class="preloader"></div>
  <div data-ix="page-load" class="page-wrapper">
    <div id="Hero" class="image-section blog-hero-section">
      <div class="wrapper w-container">
        <div id="slide1v" class="page-hero">
          <div data-collapse="medium" data-animation="default" data-duration="400" class="nav-bar w-nav">
            <div class="wrapper w-container"><a href="<?php echo get_site_url(); ?>" class="logo w-nav-brand"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_272980421"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_image_272980421','image-src'); ?>" width="200" height="63" alt="<?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_image_272980421','image-alt'); ?>" class="image-4" srcset="<?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_image_272980421','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a>
              <nav role="navigation" class="nav-menu w-nav-menu">
                <div data-delay="0" data-hover="1" class="w-dropdown">
                  <div class="nav-link w-dropdown-toggle">
                    <div class="dropdown-icon w-icon-dropdown-toggle"></div>
                    <div>Properties</div>
                  </div>
                  <nav class="dropdown w-dropdown-list"><a href="<?php echo udesly_get_permalink_by_slug('park-hyatt-st-kitts'); ?>" class="dropdown-link w-dropdown-link">Park Hyatt St. Kitts</a><a href="<?php echo udesly_get_permalink_by_slug('cabrits-resort-kempinski'); ?>" class="dropdown-link w-dropdown-link">Cabrits Resort Kempinski</a><a href="<?php echo udesly_get_permalink_by_slug('six-senses-st-kitts'); ?>" class="dropdown-link w-dropdown-link">Six Senses St. Kitts</a></nav>
                </div><a href="<?php echo udesly_get_permalink_by_slug('about-range-developments'); ?>" class="nav-link w-nav-link">About Us</a><a href="<?php echo udesly_get_permalink_by_slug('media-coverage'); ?>" class="nav-link w-nav-link">Media</a><a href="<?php echo udesly_get_permalink_by_slug('blog'); ?>" class="nav-link w-nav-link w--current">Blog</a><a href="<?php echo udesly_get_permalink_by_slug('citizenship-by-investment'); ?>" class="button white-button nav-bar-button w-button">Invest in Citizenship</a></nav>
              <div class="menu-button w-nav-button">
                <div class="w-icon-nav-menu"></div>
              </div>
            </div>
          </div>
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1611604743"><?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_text_1611604743','text'); ?></udesly-fe>
        </div>
        <div class="hero-social">
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-436807571"><?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_text_-436807571','text'); ?></udesly-fe>
          <div class="hero-social-line"></div>
          <div class="social-icons"><a href="https://www.instagram.com/range_developments/" target="_blank" class="social-link w-inline-block"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-1288260099"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_image_-1288260099','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_image_-1288260099','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_image_-1288260099','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a><a href="https://twitter.com/RangeDevelop?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank" class="social-link w-inline-block"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-790717732"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_image_-790717732','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_image_-790717732','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_image_-790717732','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a><a href="https://www.facebook.com/rangedevelopments/" target="_blank" class="social-link w-inline-block"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_446961179"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_image_446961179','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_image_446961179','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_image_446961179','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a><a href="https://www.linkedin.com/company/range-developments/" target="_blank" class="social-link w-inline-block"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_1398343091"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_image_1398343091','image-src'); ?>" width="16" height="16" alt="<?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_image_1398343091','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_image_1398343091','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a></div>
        </div>
      </div>
    </div>
    <div id="Features" class="section blog-section">
      <div class="vertical-header-wrapper vertical-header-blog">
        <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_43243734"><?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_text_43243734','text'); ?></udesly-fe>
      </div>
      <div class="wrapper w-container">
        <div class="blog">
			<?php
			get_posts();
			if ( have_posts() ) :
?>

            <div class="blog-posts w-dyn-list">
                <?php
				/* Start the Loop */
				while ( have_posts() ) : the_post();
					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/post/content', get_post_format() );

				endwhile;
?>
            </div>
                <?php
				the_posts_pagination( array(
					'prev_text' => 'prev<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
					'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'range-developments' ) . '</span>next',
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'range-developments' ) . ' </span>'
				) );

			else :
?>
            <div class="w-dyn-empty">
                <div>No items found.</div>
            </div>
            <?php

			endif;
			?>
          <div class="sidebar">
            <div class="sidebar-block">
              <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-2133354976"><?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_text_-2133354976','text'); ?></udesly-fe>
              <div class="w-dyn-list">
                  <div class="w-dyn-items">
                  <?php
				  $categories = get_categories( array(
					  'orderby' => 'name',
					  'exclude' => '5',
					  'order'   => 'ASC'
				  ) );

				  foreach( $categories as $category ) {
					  $category_link = sprintf(
						  '<a href="%1$s" alt="%2$s" class="sidebar-category-link w-inline-block"><div>%3$s</div></a>',
						  esc_url( get_category_link( $category->term_id ) ),
						  esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ),
						  esc_html( $category->name )
					  );
					  ?>
                          <div class="w-dyn-item">
                              <?php echo $category_link ?>
                          </div>
                      <?php
				  }
				  ?>
                </div>


              </div>
            </div>
            <div class="sidebar-block">
              <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_3201691"><?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_text_3201691','text'); ?></udesly-fe>
              <div class="w-dyn-list">
                <div class="w-dyn-items">

					<?php
					$recent_posts = wp_get_recent_posts(array(
						'numberposts' => 1, // Number of recent posts thumbnails to display
						'category' => 5, // Number of recent posts thumbnails to display
						'post_status' => 'publish' // Show only the published posts
					));
					foreach($recent_posts as $post) :
						?>
                        <div class="footer-posts w-dyn-list">
                            <div class="w-dyn-items">
                                <div class="footer-post w-dyn-item"><a href="#"
                                                                       style="background-image:url(&quot;<?php echo wp_get_attachment_thumb_url( get_post_thumbnail_id($post['ID']) ); ?>&quot;)"
                                                                       class="footer-post-image w-inline-block"></a>
                                    <div>
                                        <div class="footer-post-date"><?php echo get_the_date( 'M d, Y', $post ) ?></div>
                                        <a href="#" class="footer-post-header"><?php echo $post['post_title'] ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
					<?php endforeach; wp_reset_query(); ?>
                </div>
              </div>
            </div>
            <div class="sidebar-block">
              <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1034971116"><?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_text_-1034971116','text'); ?></udesly-fe>
              <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-321092179"><?php echo udesly_get_frontend_editor_content('blog_udesly_frontend_editor_text_-321092179','text'); ?></udesly-fe>
              <div class="w-form">
                <form id="email-form" name="email-form" data-name="Email Form" class="subscribe-form"><input type="email" class="input w-input" maxlength="256" name="email" data-name="Email" placeholder="Your email address" id="email" required><input type="submit" value="Subscribe" data-wait="Please wait..." class="button subscribe-button w-button"></form>
                <div class="form-success w-form-done">
                  <div>Thank you! Your submission has been received!</div>
                </div>
                <div class="form-error w-form-fail">
                  <div>Oops! Something went wrong while submitting the form.</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php get_footer(); ?>
</body></html>