
<div class="image-section footer-section">
    <div class="wrapper w-container">
        <div class="footer w-clearfix"><a href="#" data-ix="scroll-up-button-hover" class="scroll-up-button w-inline-block"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-761758884"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski-const-photos_udesly_frontend_editor_image_-761758884','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski-const-photos_udesly_frontend_editor_image_-761758884','image-alt'); ?>" class="scroll-arrow-icon" srcset="<?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski-const-photos_udesly_frontend_editor_image_-761758884','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a>
            <div class="footer-about"><a href="<?php echo get_site_url(); ?>" class="footer-logo w-nav-brand"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-719241259"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski-const-photos_udesly_frontend_editor_image_-719241259','image-src'); ?>" width="325" alt="<?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski-const-photos_udesly_frontend_editor_image_-719241259','image-alt'); ?>" class="image-5" srcset="<?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski-const-photos_udesly_frontend_editor_image_-719241259','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a>
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_861432813"><?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski-const-photos_udesly_frontend_editor_text_861432813','text'); ?></udesly-fe>
                <div class="social-icons footer-icons"><a href="https://www.instagram.com/range_developments/" target="_blank" class="social-link w-inline-block"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-1288260099"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski-const-photos_udesly_frontend_editor_image_-1288260099','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski-const-photos_udesly_frontend_editor_image_-1288260099','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski-const-photos_udesly_frontend_editor_image_-1288260099','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a><a href="https://twitter.com/RangeDevelop?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank" class="social-link w-inline-block"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-790717732"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski-const-photos_udesly_frontend_editor_image_-790717732','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski-const-photos_udesly_frontend_editor_image_-790717732','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski-const-photos_udesly_frontend_editor_image_-790717732','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a><a href="https://www.facebook.com/rangedevelopments/" target="_blank" class="social-link w-inline-block"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_446961179"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski-const-photos_udesly_frontend_editor_image_446961179','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski-const-photos_udesly_frontend_editor_image_446961179','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski-const-photos_udesly_frontend_editor_image_446961179','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a><a href="https://www.linkedin.com/company/range-developments/" target="_blank" class="social-link w-inline-block"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_1398343091"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski-const-photos_udesly_frontend_editor_image_1398343091','image-src'); ?>" width="15" alt="<?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski-const-photos_udesly_frontend_editor_image_1398343091','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski-const-photos_udesly_frontend_editor_image_1398343091','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a></div>
            </div>
            <div class="footer-links">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-490475080"><?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski-const-photos_udesly_frontend_editor_text_-490475080','text'); ?></udesly-fe><a href="<?php echo get_site_url(); ?>" class="footer-link">Properties</a><a href="<?php echo udesly_get_permalink_by_slug('about-range-developments'); ?>" class="footer-link">About Us</a><a href="<?php echo udesly_get_permalink_by_slug('media-coverage'); ?>" class="footer-link">Media</a><a href="<?php echo udesly_get_permalink_by_slug('citizenship-by-investment'); ?>" class="footer-link">What is CBI?</a><a href="<?php echo udesly_get_permalink_by_slug('blog'); ?>" class="footer-link">Blog</a></div>
            <div class="footer-links">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-46620531"><?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski-const-photos_udesly_frontend_editor_text_-46620531','text'); ?></udesly-fe><a href="mailto:info@rangedevelopments.com?subject=Website%20Contact" class="footer-link" data-ix="show-call-back-popup">Contact Us</a><a href="<?php echo udesly_get_permalink_by_slug('citizenship-by-investment'); ?>" class="footer-link">Cost of CBI</a><a href="<?php echo udesly_get_permalink_by_slug('about-range-developments'); ?>" class="footer-link">FAQs</a></div>
            <div class="footer-blog">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1847971717"><?php echo udesly_get_frontend_editor_content('cabrits-resort-kempinski_udesly_frontend_editor_text_-1847971717','text'); ?></udesly-fe>

				<?php
				$recent_posts = wp_get_recent_posts(array(
					'numberposts' => 1, // Number of recent posts thumbnails to display
					'post_status' => 'publish' // Show only the published posts
				));
				foreach($recent_posts as $post) :
					?>
                    <div class="footer-posts w-dyn-list">
                        <div class="w-dyn-items">
                            <div class="footer-post w-dyn-item"><a href="#"
                                                                   style="background-image:url(&quot;<?php echo wp_get_attachment_thumb_url( get_post_thumbnail_id($post['ID']) ); ?>&quot;)"
                                                                   class="footer-post-image w-inline-block"></a>
                                <div>
                                    <div class="footer-post-date"><?php echo get_the_date( 'M d, Y', $post ) ?></div>
                                    <a href="#" class="footer-post-header"><?php echo $post['post_title'] ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
				<?php endforeach; wp_reset_query(); ?>
                <a href="/blog/" class="footer-link blog-link">View All Posts</a>

            </div>
            <div class="footer-copyright">
                <div>Citizenship by Investment</div>
                <div class="copyright-text">&#xA9; Range Developments, <?=date('Y'); ?>. All rights reserved&#xA0; &#xA0;|&#xA0; &#xA0;Privacy Policy</div>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">var $ = window.jQuery;</script><script src="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>js/range-developments.js?v=1542837570" type="text/javascript"></script>
<!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->