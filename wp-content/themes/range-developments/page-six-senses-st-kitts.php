<?php defined( 'ABSPATH' ) || exit; ?><!DOCTYPE html><!--  Last Published: Tue Nov 20 2018 19:51:23 GMT+0000 (UTC)  --><html data-wf-page="5b4f4f6a6c24e5cc3ee09eb8" data-wf-site="5ab00568c82e2061c2a05a58"><head>
  <meta charset="utf-8">
  
  
  
  
  
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>css/normalize.css?v=1542837576" rel="stylesheet" type="text/css">
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>css/components.css?v=1542837576" rel="stylesheet" type="text/css">
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>css/range-developments.css?v=1542837576" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Roboto:regular,700"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>images/favicon.png?v=1542837576" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>images/webclip.png?v=1542837576" rel="apple-touch-icon">
  <meta name="ahrefs-site-verification" content="714c4b625dd53181fcfed919e97e4e4ae294cc8fb251d1cb911b02256a21e297">
  <meta name="google-site-verification" content="7_G9HvmS0Ee9hApkUO-UoLrk6eJx75oDWDroa2R4otE">
  <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
  <!--  Global site tag (gtag.js) - Google Analytics  -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-4118182-47"></script>
  <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-4118182-47');
</script>
  <script src="https://global.localizecdn.com/localize.js"></script>
  <script>!function(a){if(!a.Localize){a.Localize={};for(var e=["translate","untranslate","phrase","initialize","translatePage","setLanguage","getLanguage","detectLanguage","getAvailableLanguages","untranslatePage","bootstrap","prefetch","on","off"],t=0;t<e.length;t++)a.Localize[e[t]]=function(){}}}(window);</script>
  <script>
 Localize.initialize({
  key: 'reyBHeN3Gieh5',
  rememberLanguage: true
 });
</script>
  <script>
window.onload = function(){
 if (Localize.getLanguage() === "ar") {
   document.getElementsByTagName("body")[0].setAttribute("dir", "rtl");
   document.getElementById("Hero").setAttribute("dir", "ltr");
   if (document.getElementById("slide1")) {document.getElementById("slide1").setAttribute("dir", "rtl");}
   if (document.getElementById("slide2")) {document.getElementById("slide2").setAttribute("dir", "rtl");}
   if (document.getElementById("hero-call-text")) {document.getElementById("hero-call-text").setAttribute("dir", "rtl");}
   if (document.getElementById("phone-number-dir")) {document.getElementById("phone-number-dir").setAttribute("dir", "ltr");}
   if (document.getElementById("Testimonials")) {document.getElementById("Testimonials").setAttribute("dir", "ltr");}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").setAttribute("dir", "rtl");}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").setAttribute("dir", "rtl");}
   document.getElementsByTagName("body")[0].style.textAlign = "right";
   document.getElementById("Hero").style.textAlign = "right";
   if (document.getElementById("slide1")) {document.getElementById("slide1").style.textAlign = "right";}
   if (document.getElementById("slide2")) {document.getElementById("slide2").style.textAlign = "right";}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").style.textAlign = "right";}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").style.textAlign = "right";}
   if ($('.vertical-header-wrapper')) {$('.vertical-header-wrapper').css('right', '0'); $('.vertical-header').css('left', '124px');}
   if (document.getElementById("wistia-bqquvi10jm-1_popover")) {document.getElementById("wistia-bqquvi10jm-1_popover").style.right = "-99999px";document.getElementById("wistia-bqquvi10jm-1_popover").style.left = "auto";}
   if (document.getElementById("wistia-bqquvi10jm-1_popover_popover_close_button")) {document.getElementById("wistia-bqquvi10jm-1_popover_popover_close_button").style.right = "-99999px";document.getElementById("wistia-bqquvi10jm-1_popover").style.left = "auto";}
 }
}
Localize.on("setLanguage", function(data) {
 if (data.to === "ar") {
   document.getElementsByTagName("body")[0].setAttribute("dir", "rtl");
   document.getElementById("Hero").setAttribute("dir", "ltr");
   if (document.getElementById("slide1")) {document.getElementById("slide1").setAttribute("dir", "rtl");}
   if (document.getElementById("slide2")) {document.getElementById("slide2").setAttribute("dir", "rtl");}
   if (document.getElementById("hero-call-text")) {document.getElementById("hero-call-text").setAttribute("dir", "rtl");}
   if (document.getElementById("phone-number-dir")) {document.getElementById("phone-number-dir").setAttribute("dir", "ltr");}
   if (document.getElementById("Testimonials")) {document.getElementById("Testimonials").setAttribute("dir", "ltr");}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").setAttribute("dir", "rtl");}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").setAttribute("dir", "rtl");}
   document.getElementsByTagName("body")[0].style.textAlign = "right";
   document.getElementById("Hero").style.textAlign = "right";
   if (document.getElementById("slide1")) {document.getElementById("slide1").style.textAlign = "right";}
   if (document.getElementById("slide2")) {document.getElementById("slide2").style.textAlign = "right";}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").style.textAlign = "right";}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").style.textAlign = "right";}
 }
 if (data.to != "ar") {
   document.getElementsByTagName("body")[0].setAttribute("dir", "ltr");
   if (document.getElementById("slide1")) {document.getElementById("slide1").setAttribute("dir", "ltr");}
   if (document.getElementById("slide2")) {document.getElementById("slide2").setAttribute("dir", "ltr");}
   if (document.getElementById("hero-call-text")) {document.getElementById("hero-call-text").setAttribute("dir", "ltr");}
   if (document.getElementById("phone-number-dir")) {document.getElementById("phone-number-dir").setAttribute("dir", "ltr");}
   if (document.getElementById("Testimonials")) {document.getElementById("Testimonials").setAttribute("dir", "ltr");}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").setAttribute("dir", "ltr");}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").setAttribute("dir", "ltr");}
   document.getElementsByTagName("body")[0].style.textAlign = "left";
   document.getElementById("Hero").style.textAlign = "left";
   if (document.getElementById("slide1")) {document.getElementById("slide1").style.textAlign = "left";}
   if (document.getElementById("slide2")) {document.getElementById("slide2").style.textAlign = "left";}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").style.textAlign = "left";}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").style.textAlign = "left";}
 }
})
</script>
<?php wp_enqueue_script("jquery"); wp_head(); ?></head>
<body class="body <?php echo " ".join( ' ', get_body_class() ); ?>" udesly-page-name="six-senses-st-kitts"><?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div data-ix="preloader" class="preloader"></div>
  <div data-ix="page-load" class="page-wrapper">
    <div class="call-back">
      <div class="call-back-wrapper">
        <div class="call-back-popup"><a href="#" data-ix="hide-call-back-popup" class="close-popup-button w-inline-block"></a>
          <div class="contact-form-wrapper w-form">
            <form id="email-form" name="email-form" data-name="Email Form" class="contact-form">
              <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-113239269"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_-113239269','text'); ?></udesly-fe><label for="name-2" class="form-label">Name</label><input type="text" class="input w-input" maxlength="256" name="name-2" data-name="Name 2" placeholder="Enter your name" id="name-2" required><label for="email-2" class="form-label">Email</label><input type="text" class="input w-input" maxlength="256" name="email-2" data-name="Email 2" placeholder="Enter your email" id="email-2" required><label for="Phone-3" class="form-label">Phone</label><input type="text" class="input w-input" maxlength="256" name="Phone-3" data-name="Phone 3" placeholder="Enter your phone" id="Phone-3" required><label for="Country-2" class="form-label">Country</label><input type="text" class="input w-input" maxlength="256" name="Country-2" data-name="Country 2" placeholder="Enter your country" id="Country-2" required><label for="Message-2" class="form-label">Message</label><textarea id="Message-2" name="Message-2" placeholder="How can we help you?" maxlength="5000" data-name="Message 2" required class="input text-area w-input"></textarea><input type="submit" value="Contact Us" data-wait="Please wait..." class="button form-button w-button"></form>
            <div class="w-form-done">
              <div>Thank you! Your submission has been received!</div>
            </div>
            <div class="w-form-fail">
              <div>Oops! Something went wrong while submitting the form.</div>
            </div>
          </div>
        </div>
        <div data-ix="hide-call-back-popup" class="popup-overlay"></div>
      </div>
      <a href="#" data-ix="show-call-back-popup" class="call-back-button w-inline-block">
        <div class="contact-popup">Contact Us</div>
      </a>
    </div>
    <div data-collapse="medium" data-animation="default" data-duration="400" class="nav-bar fixed-nav-bar w-nav">
      <div class="wrapper w-container"><a href="<?php echo get_site_url(); ?>" class="logo w-nav-brand"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-719241259"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-719241259','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-719241259','image-alt'); ?>" class="image-8" srcset="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-719241259','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a>
        <nav role="navigation" class="nav-menu pop-up w-nav-menu">
          <div data-delay="0" data-hover="1" class="w-dropdown">
            <div class="nav-link pop-up w-dropdown-toggle">
              <div class="dropdown-icon w-icon-dropdown-toggle"></div>
              <div>Properties</div>
            </div>
            <nav class="dropdown w-dropdown-list"><a href="<?php echo udesly_get_permalink_by_slug('park-hyatt-st-kitts'); ?>" class="dropdown-link w-dropdown-link">Park Hyatt St. Kitts</a><a href="<?php echo udesly_get_permalink_by_slug('cabrits-resort-kempinski'); ?>" class="dropdown-link w-dropdown-link">Cabrits Resort Kempinski</a><a href="<?php echo udesly_get_permalink_by_slug('six-senses-st-kitts'); ?>" class="dropdown-link w-dropdown-link w--current">Six Senses St. Kitts</a></nav>
          </div><a href="<?php echo udesly_get_permalink_by_slug('about-range-developments'); ?>" class="nav-link pop-up w-nav-link">About Us</a><a href="<?php echo udesly_get_permalink_by_slug('media-coverage'); ?>" class="nav-link pop-up w-nav-link">Media</a><a href="<?php echo udesly_get_permalink_by_slug('blog'); ?>" class="nav-link pop-up w-nav-link">Blog</a><a href="<?php echo udesly_get_permalink_by_slug('citizenship-by-investment'); ?>" class="button fixed-nav-bar-button w-button">Invest in Citizenship</a></nav>
        <div class="menu-button fixed-menu-button w-nav-button">
          <div class="w-icon-nav-menu"></div>
        </div>
      </div>
    </div>
    <div id="Hero" data-ix="show-nav-bar-on-scroll" class="image-section hero-section">
      <div data-collapse="medium" data-animation="default" data-duration="400" class="nav-bar w-nav">
        <div class="wrapper w-container"><a href="<?php echo get_site_url(); ?>" class="logo w-nav-brand"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_272980421"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_272980421','image-src'); ?>" width="200" height="63" alt="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_272980421','image-alt'); ?>" class="image-4" srcset="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_272980421','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a>
          <nav role="navigation" class="nav-menu w-nav-menu">
            <div data-delay="0" data-hover="1" class="w-dropdown">
              <div class="nav-link w-dropdown-toggle">
                <div class="dropdown-icon w-icon-dropdown-toggle"></div>
                <div>Properties</div>
              </div>
              <nav class="dropdown w-dropdown-list"><a href="<?php echo udesly_get_permalink_by_slug('park-hyatt-st-kitts'); ?>" class="dropdown-link w-dropdown-link">Park Hyatt St. Kitts</a><a href="<?php echo udesly_get_permalink_by_slug('cabrits-resort-kempinski'); ?>" class="dropdown-link w-dropdown-link">Cabrits Resort Kempinski</a><a href="<?php echo udesly_get_permalink_by_slug('six-senses-st-kitts'); ?>" class="dropdown-link w-dropdown-link w--current">Six Senses St. Kitts</a></nav>
            </div><a href="<?php echo udesly_get_permalink_by_slug('about-range-developments'); ?>" class="nav-link w-nav-link">About Us</a><a href="<?php echo udesly_get_permalink_by_slug('media-coverage'); ?>" class="nav-link w-nav-link">Media</a><a href="<?php echo udesly_get_permalink_by_slug('blog'); ?>" class="nav-link w-nav-link">Blog</a><a href="<?php echo udesly_get_permalink_by_slug('citizenship-by-investment'); ?>" class="button white-button nav-bar-button w-button">Invest in Citizenship</a></nav>
          <div class="menu-button w-nav-button">
            <div class="w-icon-nav-menu"></div>
          </div>
        </div>
      </div>
      <div class="hero-call-text-wrapper">
        <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1577052701"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_-1577052701','text'); ?></udesly-fe>
      </div>
      <div data-delay="6000" data-animation="slide" data-autoplay="1" data-duration="600" data-infinite="1" class="hero-slider w-slider">
        <div class="w-slider-mask">
          <div class="slide-1 six-senses-st-kitts w-slide">
            <div class="wrapper slide-wrapper w-container">
              <div id="slide1" class="slide-content no-gallery">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1114534156"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_1114534156','text'); ?></udesly-fe>
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1879230322"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_-1879230322','text'); ?></udesly-fe>
                <div class="button ghost-white-button video w-embed w-script">
                  <script src="https://fast.wistia.com/embed/medias/bqquvi10jm.jsonp" async></script>
                  <script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_bqquvi10jm popover=true popoverContent=link" style="display:inline;position:relative"><a href="#" style="color:white; text-decoration:none;">Watch Video</a></span></div><a data-ix="fade-up-4" href="<?php echo udesly_get_permalink_by_slug('downloads'); ?>" class="button hero-button margin-left w-button">Brochures &amp; Forms</a></div>
            </div>
          </div>
        </div>
        <div class="slider-arrow arrow-left off w-hidden-small w-hidden-tiny w-slider-arrow-left"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-265916240"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-265916240','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-265916240','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-265916240','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></div>
        <div class="slider-arrow arrow-right off w-hidden-small w-hidden-tiny w-slider-arrow-right"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_550220069"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_550220069','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_550220069','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_550220069','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></div>
        <div class="w-hidden-main w-hidden-medium w-hidden-small w-hidden-tiny w-slider-nav w-round"></div>
      </div>
      <div class="hero-bottom">
        <div class="wrapper hero-bottom-wrapper w-container">
          <div class="hero-social">
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-436807571"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_-436807571','text'); ?></udesly-fe>
            <div class="hero-social-line"></div>
            <div class="social-icons"><a href="https://www.instagram.com/range_developments/" target="_blank" class="social-link w-inline-block"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-1288260099"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-1288260099','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-1288260099','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-1288260099','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a><a href="https://twitter.com/RangeDevelop?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank" class="social-link w-inline-block"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-790717732"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-790717732','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-790717732','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-790717732','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a><a href="https://www.facebook.com/rangedevelopments/" target="_blank" class="social-link w-inline-block"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_446961179"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_446961179','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_446961179','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_446961179','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a><a href="https://www.linkedin.com/company/range-developments/" target="_blank" class="social-link w-inline-block"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_1398343091"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_1398343091','image-src'); ?>" width="16" height="16" alt="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_1398343091','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_1398343091','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a></div>
          </div><a href="#Features" data-ix="scroll-down-button-hover" class="scroll-down-button w-inline-block"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-227153291"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-227153291','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-227153291','image-alt'); ?>" class="scroll-arrow-icon" srcset="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-227153291','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a></div>
      </div>
    </div>
    <div id="Features" class="section no-paddings">
      <div class="feature-cells">
        <div class="feature-cell">
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-656463689"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_-656463689','text'); ?></udesly-fe>
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_812102047"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_812102047','text'); ?></udesly-fe>
        </div>
        <div class="feature-cell">
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_320590138"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_320590138','text'); ?></udesly-fe>
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_744155510"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_744155510','text'); ?></udesly-fe>
        </div>
        <div class="feature-cell">
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-196903481"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_-196903481','text'); ?></udesly-fe>
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_647806366"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_647806366','text'); ?></udesly-fe>
        </div>
        <div class="feature-cell">
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-372576535"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_-372576535','text'); ?></udesly-fe>
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_2143154150"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_2143154150','text'); ?></udesly-fe>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="vertical-header-wrapper">
        <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_962191745"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_962191745','text'); ?></udesly-fe>
      </div>
      <div class="wrapper w-container">
        <div class="side-feature">
          <div class="side-feature-image"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_68140225"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_68140225','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_68140225','image-alt'); ?>" class="image-10" srcset="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_68140225','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></div>
          <div class="side-feature-content">
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1730811550"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_-1730811550','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-754817913"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_-754817913','text'); ?></udesly-fe>
            <div class="side-feature-mini-wrapper"></div><a href="#" class="lightbox-link w-inline-block w-lightbox"><script type="application/json" class="w-json">{"items":[{"type":"image","_id":"5aea6fd72ea710769615d452","fileName":"DJI_0098.JPG","origFileName":"DJI_0098.JPG","width":900,"height":580,"fileSize":255284,"url":"<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>images/DJI_0098.JPG"},{"type":"image","_id":"5aea7017e57864ca3ad0c8e6","fileName":"DSCN8500.JPG","origFileName":"DSCN8500.JPG","width":900,"height":580,"fileSize":162108,"url":"images/DSCN8500.JPG"},{"type":"image","_id":"5aea70176f1571389c5f1079","fileName":"DSCN8496.JPG","origFileName":"DSCN8496.JPG","width":900,"height":580,"fileSize":145227,"url":"images/DSCN8496.JPG"},{"type":"image","_id":"5aea701749aae428b509b791","fileName":"DSCN8504.JPG","origFileName":"DSCN8504.JPG","width":900,"height":580,"fileSize":184179,"url":"images/DSCN8504.JPG"},{"type":"image","_id":"5aea70186e89981c9922856d","fileName":"DJI_0103.JPG","origFileName":"DJI_0103.JPG","width":900,"height":580,"fileSize":262597,"url":"images/DJI_0103.JPG"}]}</script></a><a href="#" class="button white w-button">See Pricing Breakdown</a></div>
        </div>
      </div>
    </div>
    <div class="image-section facts-bg-section">
      <div class="wrapper w-container">
        <div class="facts">
          <div data-ix="fade-up-1" class="fact-item">
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_763440520"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_763440520','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_694529087"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_694529087','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1022709457"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_1022709457','text'); ?></udesly-fe>
          </div>
          <div data-ix="fade-up-2" class="fact-item">
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1650944201"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_1650944201','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_997180212"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_997180212','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_912586115"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_912586115','text'); ?></udesly-fe>
          </div>
          <div data-ix="fade-up-3" class="fact-item">
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1756519414"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_-1756519414','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-2100768735"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_-2100768735','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-352705353"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_-352705353','text'); ?></udesly-fe>
          </div>
          <div data-ix="fade-up-4" class="fact-item">
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-869015733"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_-869015733','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1458929502"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_1458929502','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1553957059"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_-1553957059','text'); ?></udesly-fe>
          </div>
        </div>
      </div>
    </div>
    <div id="Pricing" class="section">
      <div class="vertical-header-wrapper">
        <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1616585661"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_1616585661','text'); ?></udesly-fe>
      </div>
      <div id="pricing" class="wrapper w-container">
        <div class="section-header-wrapper">
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_415858176"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_415858176','text'); ?></udesly-fe>
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1029799763"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_1029799763','text'); ?></udesly-fe>
        </div>
        <div class="pricing-tables not-same-height">
          <div data-ix="fade-up-1" class="pricing-table">
            <div class="card-corner">
              <div>For Family of 4</div>
              <div class="card-corner-line"></div>
            </div>
            <div class="price">$314,500<span class="text-span">// Total</span></div>
            <div class="pricing-table-list">
              <div class="pricing-table-feature grey">Cost of share: $220,000</div>
              <div class="pricing-table-feature">Government fees for the main applicant, a spouse and 2 dependents: $75,000</div>
              <div class="pricing-table-feature grey">Due Diligence fees: $19,500</div>
              <div class="pricing-table-feature"></div>
              <div class="pricing-table-feature grey"></div>
            </div><a href="#" class="button w-button" data-ix="show-call-back-popup">Get Started</a><a href="<?php echo udesly_get_permalink_by_slug('downloads'); ?>" class="forms-link">Brochures and Forms</a></div>
          <div class="image-bg-right"></div>
        </div>
        <div class="pricing-text">In doubt? Check out <a href="#FAQ" class="text-link">FAQ</a> and don&#x2019;t hesitate to <a href="#" class="text-link" data-ix="show-call-back-popup">contact us</a>.</div>
      </div>
    </div>
    <div id="Testimonials" class="image-section testimonials-section">
      <div data-animation="slide" data-duration="600" data-infinite="1" class="testimonials w-slider">
        <div class="w-slider-mask">
          <div class="w-slide">
            <div class="review-slide"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-1951273038"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-1951273038','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-1951273038','image-alt'); ?>" class="review-avatar" srcset="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-1951273038','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe>
              <div class="review-content">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1606380084"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_1606380084','text'); ?></udesly-fe>
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1391193530"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_1391193530','text'); ?></udesly-fe>
                <div class="review-author-info">// Prime Minister of St. Kitts and Nevis</div>
              </div>
            </div>
          </div>
          <div class="w-slide">
            <div class="review-slide"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_118027746"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_118027746','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_118027746','image-alt'); ?>" class="review-avatar" srcset="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_118027746','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe>
              <div class="review-content">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1380442496"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_1380442496','text'); ?></udesly-fe>
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_405294212"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_405294212','text'); ?></udesly-fe>
                <div class="review-author-info">// Prime Minister of DOMINICA</div>
              </div>
            </div>
          </div>
        </div>
        <div class="slider-arrow arrow-left w-slider-arrow-left"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-265916240"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-265916240','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-265916240','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-265916240','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></div>
        <div class="slider-arrow arrow-right w-slider-arrow-right"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_550220069"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_550220069','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_550220069','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_550220069','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></div>
        <div class="testimonoals-nav w-hidden-main w-hidden-medium w-slider-nav w-round"></div>
      </div>
    </div>
    <div id="FAQ" class="section">
      <div class="vertical-header-wrapper">
        <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1275893559"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_1275893559','text'); ?></udesly-fe>
      </div>
      <div class="wrapper w-container">
        <div class="faq reversed">
          <div class="faq-list">
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1171564786"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_-1171564786','text'); ?></udesly-fe>
            <a href="#" data-ix="show-answer" class="question-container w-inline-block">
              <div class="question">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-714397765"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_-714397765','text'); ?></udesly-fe>
                <div class="plus-icon">
                  <div class="plus-icon-v-line"></div>
                  <div class="plus-icon-h-line"></div>
                </div>
                <div class="question-line"></div>
              </div>
              <div data-ix="hide-on-load" class="answer">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1369803914"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_1369803914','text'); ?></udesly-fe>
              </div>
            </a>
            <a href="#" data-ix="show-answer" class="question-container w-inline-block">
              <div class="question">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_2055047029"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_2055047029','text'); ?></udesly-fe>
                <div class="plus-icon">
                  <div class="plus-icon-v-line"></div>
                  <div class="plus-icon-h-line"></div>
                </div>
                <div class="question-line"></div>
              </div>
              <div data-ix="hide-on-load" class="answer">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_954629603"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_954629603','text'); ?></udesly-fe>
              </div>
            </a>
            <a href="#" data-ix="show-answer" class="question-container w-inline-block">
              <div class="question">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1430625872"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_1430625872','text'); ?></udesly-fe>
                <div class="plus-icon">
                  <div class="plus-icon-v-line"></div>
                  <div class="plus-icon-h-line"></div>
                </div>
                <div class="question-line"></div>
              </div>
              <div data-ix="hide-on-load" class="answer">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1891742070"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_1891742070','text'); ?></udesly-fe>
              </div>
            </a>
            <a href="#" data-ix="show-answer" class="question-container w-inline-block">
              <div class="question">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_307733963"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_307733963','text'); ?></udesly-fe>
                <div class="plus-icon">
                  <div class="plus-icon-v-line"></div>
                  <div class="plus-icon-h-line"></div>
                </div>
                <div class="question-line"></div>
              </div>
              <div data-ix="hide-on-load" class="answer">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_155419"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_155419','text'); ?></udesly-fe>
              </div>
            </a>
            <a href="#" data-ix="show-answer" class="question-container w-inline-block">
              <div class="question">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1259042582"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_-1259042582','text'); ?></udesly-fe>
                <div class="plus-icon">
                  <div class="plus-icon-v-line"></div>
                  <div class="plus-icon-h-line"></div>
                </div>
                <div class="question-line"></div>
              </div>
              <div data-ix="hide-on-load" class="answer">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_511697560"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_511697560','text'); ?></udesly-fe>
              </div>
            </a>
            <a href="#" data-ix="show-answer" class="question-container w-inline-block">
              <div class="question">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-578237864"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_-578237864','text'); ?></udesly-fe>
                <div class="plus-icon">
                  <div class="plus-icon-v-line"></div>
                  <div class="plus-icon-h-line"></div>
                </div>
                <div class="question-line"></div>
              </div>
              <div data-ix="hide-on-load" class="answer">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1553631933"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_-1553631933','text'); ?></udesly-fe>
              </div>
            </a>
            <a href="#" data-ix="show-answer" class="question-container w-inline-block">
              <div class="question">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1908281743"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_-1908281743','text'); ?></udesly-fe>
                <div class="plus-icon">
                  <div class="plus-icon-v-line"></div>
                  <div class="plus-icon-h-line"></div>
                </div>
                <div class="question-line"></div>
              </div>
              <div data-ix="hide-on-load" class="answer">
                <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1889590582"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_1889590582','text'); ?></udesly-fe>
              </div>
            </a>
          </div>
          <div class="faq-image"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-1097329898"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-1097329898','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-1097329898','image-alt'); ?>" class="w-hidden-small w-hidden-tiny" srcset="<?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_image_-1097329898','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe>
            <div class="faq-contact-text">Still have a question? <a href="tel:+9714-325-3447" class="text-link" data-ix="show-call-back-popup">Contact us</a>.</div>
          </div>
        </div>
      </div>
    </div>
    <div id="Team" class="section small-padding">
      <div class="vertical-header-wrapper">
        <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-173100095"><?php echo udesly_get_frontend_editor_content('six-senses-st-kitts_udesly_frontend_editor_text_-173100095','text'); ?></udesly-fe>
      </div>
      <div class="wrapper w-container">
        <div class="section-header-wrapper"></div>
        <div class="media">
          <div data-ix="fade-up-1" class="team-member-card">
            <div class="card-corner">
              <div>Park Hyatt St. Kitts</div>
              <div class="card-corner-line"></div>
            </div>
            <div class="square-resort"></div>
            <div class="team-member-text location-text">A world-renowned 5 star resort located on the beautiful Caribbean island of St. Kitts &amp; Nevis.</div><a data-ix="fade-up-4" href="<?php echo udesly_get_permalink_by_slug('park-hyatt-st-kitts'); ?>" class="button ghost-white-button on-white w-button">Learn More</a></div>
          <div class="team-member-card">
            <div class="card-corner">
              <div>Cabrits Resort Kempinski, Dominica</div>
              <div class="card-corner-line"></div>
            </div>
            <div class="square-resort kempinski"></div>
            <div class="team-member-text location-text">Cabrits Resort Kempinski, Dominica, will be a hideaway resort that will rank among the best in its class in the Caribbean.</div><a data-ix="fade-up-4" href="<?php echo udesly_get_permalink_by_slug('cabrits-resort-kempinski'); ?>" class="button ghost-white-button on-white w-button">Learn More</a></div>
          <div data-ix="fade-up-1" class="team-member-card">
            <div class="card-corner">
              <div>Six Senses St. Kitts</div>
              <div class="card-corner-line"></div>
            </div>
            <div class="square-resort six-senses"></div>
            <div class="team-member-text location-text">Located on the picturesque La Vallee area on the island of St. Kitts &amp; Nevis, this captivating resort is scheduled for opening in 2021.</div><a data-ix="fade-up-4" href="<?php echo udesly_get_permalink_by_slug('six-senses-st-kitts'); ?>" class="button ghost-white-button on-white w-button w--current">Learn More</a></div>
        </div>
      </div>
    </div>
	<?php get_footer(); ?>
<?php wp_footer(); ?><?php endwhile; endif; ?></body></html>