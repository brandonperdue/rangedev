<?php defined( 'ABSPATH' ) || exit; ?><!DOCTYPE html><!--  Last Published: Tue Nov 20 2018 19:51:23 GMT+0000 (UTC)  --><html data-wf-page="5b4f4f6a6c24e51c7ce09e1b" data-wf-site="5ab00568c82e2061c2a05a58"><head>
  <meta charset="utf-8">
  
  
  
  
  
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>css/normalize.css?v=1542837568" rel="stylesheet" type="text/css">
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>css/components.css?v=1542837568" rel="stylesheet" type="text/css">
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>css/range-developments.css?v=1542837568" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Roboto:regular,700"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>images/favicon.png?v=1542837568" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>images/webclip.png?v=1542837568" rel="apple-touch-icon">
  <meta name="ahrefs-site-verification" content="714c4b625dd53181fcfed919e97e4e4ae294cc8fb251d1cb911b02256a21e297">
  <meta name="google-site-verification" content="7_G9HvmS0Ee9hApkUO-UoLrk6eJx75oDWDroa2R4otE">
  <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
  <!--  Global site tag (gtag.js) - Google Analytics  -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-4118182-47"></script>
  <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-4118182-47');
</script>
  <script src="https://global.localizecdn.com/localize.js"></script>
  <script>!function(a){if(!a.Localize){a.Localize={};for(var e=["translate","untranslate","phrase","initialize","translatePage","setLanguage","getLanguage","detectLanguage","getAvailableLanguages","untranslatePage","bootstrap","prefetch","on","off"],t=0;t<e.length;t++)a.Localize[e[t]]=function(){}}}(window);</script>
  <script>
 Localize.initialize({
  key: 'reyBHeN3Gieh5',
  rememberLanguage: true
 });
</script>
  <script>
window.onload = function(){
 if (Localize.getLanguage() === "ar") {
   document.getElementsByTagName("body")[0].setAttribute("dir", "rtl");
   document.getElementById("Hero").setAttribute("dir", "ltr");
   if (document.getElementById("slide1")) {document.getElementById("slide1").setAttribute("dir", "rtl");}
   if (document.getElementById("slide2")) {document.getElementById("slide2").setAttribute("dir", "rtl");}
   if (document.getElementById("hero-call-text")) {document.getElementById("hero-call-text").setAttribute("dir", "rtl");}
   if (document.getElementById("phone-number-dir")) {document.getElementById("phone-number-dir").setAttribute("dir", "ltr");}
   if (document.getElementById("Testimonials")) {document.getElementById("Testimonials").setAttribute("dir", "ltr");}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").setAttribute("dir", "rtl");}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").setAttribute("dir", "rtl");}
   document.getElementsByTagName("body")[0].style.textAlign = "right";
   document.getElementById("Hero").style.textAlign = "right";
   if (document.getElementById("slide1")) {document.getElementById("slide1").style.textAlign = "right";}
   if (document.getElementById("slide2")) {document.getElementById("slide2").style.textAlign = "right";}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").style.textAlign = "right";}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").style.textAlign = "right";}
   if ($('.vertical-header-wrapper')) {$('.vertical-header-wrapper').css('right', '0'); $('.vertical-header').css('left', '124px');}
   if (document.getElementById("wistia-bqquvi10jm-1_popover")) {document.getElementById("wistia-bqquvi10jm-1_popover").style.right = "-99999px";document.getElementById("wistia-bqquvi10jm-1_popover").style.left = "auto";}
   if (document.getElementById("wistia-bqquvi10jm-1_popover_popover_close_button")) {document.getElementById("wistia-bqquvi10jm-1_popover_popover_close_button").style.right = "-99999px";document.getElementById("wistia-bqquvi10jm-1_popover").style.left = "auto";}
 }
}
Localize.on("setLanguage", function(data) {
 if (data.to === "ar") {
   document.getElementsByTagName("body")[0].setAttribute("dir", "rtl");
   document.getElementById("Hero").setAttribute("dir", "ltr");
   if (document.getElementById("slide1")) {document.getElementById("slide1").setAttribute("dir", "rtl");}
   if (document.getElementById("slide2")) {document.getElementById("slide2").setAttribute("dir", "rtl");}
   if (document.getElementById("hero-call-text")) {document.getElementById("hero-call-text").setAttribute("dir", "rtl");}
   if (document.getElementById("phone-number-dir")) {document.getElementById("phone-number-dir").setAttribute("dir", "ltr");}
   if (document.getElementById("Testimonials")) {document.getElementById("Testimonials").setAttribute("dir", "ltr");}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").setAttribute("dir", "rtl");}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").setAttribute("dir", "rtl");}
   document.getElementsByTagName("body")[0].style.textAlign = "right";
   document.getElementById("Hero").style.textAlign = "right";
   if (document.getElementById("slide1")) {document.getElementById("slide1").style.textAlign = "right";}
   if (document.getElementById("slide2")) {document.getElementById("slide2").style.textAlign = "right";}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").style.textAlign = "right";}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").style.textAlign = "right";}
 }
 if (data.to != "ar") {
   document.getElementsByTagName("body")[0].setAttribute("dir", "ltr");
   if (document.getElementById("slide1")) {document.getElementById("slide1").setAttribute("dir", "ltr");}
   if (document.getElementById("slide2")) {document.getElementById("slide2").setAttribute("dir", "ltr");}
   if (document.getElementById("hero-call-text")) {document.getElementById("hero-call-text").setAttribute("dir", "ltr");}
   if (document.getElementById("phone-number-dir")) {document.getElementById("phone-number-dir").setAttribute("dir", "ltr");}
   if (document.getElementById("Testimonials")) {document.getElementById("Testimonials").setAttribute("dir", "ltr");}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").setAttribute("dir", "ltr");}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").setAttribute("dir", "ltr");}
   document.getElementsByTagName("body")[0].style.textAlign = "left";
   document.getElementById("Hero").style.textAlign = "left";
   if (document.getElementById("slide1")) {document.getElementById("slide1").style.textAlign = "left";}
   if (document.getElementById("slide2")) {document.getElementById("slide2").style.textAlign = "left";}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").style.textAlign = "left";}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").style.textAlign = "left";}
 }
})
</script>
<?php wp_enqueue_script("jquery"); wp_head(); ?></head>
<body class="<?php echo " ".join( ' ', get_body_class() ); ?>" udesly-page-name="401">
  <div data-ix="page-load" class="page-wrapper">
    <div class="utility-page-wrap">
      <div class="utility-page-content w-password-page w-form">
        <form method="post" action="/.wf_auth" class="utility-page-form w-password-page"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-557046209"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('401_udesly_frontend_editor_image_-557046209','image-src'); ?>" width="56" alt="<?php echo udesly_get_frontend_editor_content('401_udesly_frontend_editor_image_-557046209','image-alt'); ?>" class="password-icon" srcset="<?php echo udesly_get_frontend_editor_content('401_udesly_frontend_editor_image_-557046209','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe>
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1904537800"><?php echo udesly_get_frontend_editor_content('401_udesly_frontend_editor_text_1904537800','text'); ?></udesly-fe>
          <div style="display:none" class="w-password-page w-embed w-script"><input type="hidden" name="path" value="&lt;%WF_FORM_VALUE_PATH%&gt;"><input type="hidden" name="page" value="&lt;%WF_FORM_VALUE_PAGE%&gt;"></div><input type="password" name="pass" placeholder="Enter your password" maxlength="256" autofocus="true" class="input password-input w-password-page w-input"><input type="submit" value="Submit" data-wait="Please wait..." class="button password-button w-password-page w-button"></form>
        <div class="form-error w-password-page w-form-fail">
          <div>Incorrect password. Please try again.</div>
        </div>
        <div style="display:none" class="w-password-page w-embed w-script">
          <script type="application/javascript">(function _handlePasswordPageOnload() {
	  if (/[?&]e=1(&|$)/.test(document.location.search)) {
	    document.querySelector('.w-password-page.w-form-fail').style.display = 'block';
	  }
	})()</script>
        </div>
      </div>
    </div>
  </div>
  
  <script type="text/javascript">var $ = window.jQuery;</script><script src="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>js/range-developments.js?v=1542837568" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->

<?php wp_footer(); ?></body></html>