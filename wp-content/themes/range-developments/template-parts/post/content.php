<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */


$postid = get_the_ID();
$categories = get_the_category();
echo wp_get_attachment_url( get_the_ID() );
?>
<div class="w-dyn-items">
    <div class="blog-post-card w-dyn-item">
        <div class="post-date">
            <div class="post-date-line"></div>
            <div class="post-date-text"><?php echo get_the_date( 'd' ) ?></div>
            <div class="post-date-month"><?php echo get_the_date( 'M' ) ?></div>
        </div>
        <div class="post-card">
            <a href="<?php echo get_category_link( $categories[0]->cat_ID ); ?>" class="blog-category post-card-category"><?php echo $categories[0]->name; ?></a>
            <a href="<?php the_permalink() ?>" style="background-image:url(&quot;<?php echo wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) ); ?>&quot;)" class="post-card-image w-inline-block"></a>
            <a href="<?php the_permalink() ?>" class="post-card-header w-inline-block">
				<?php ;
				the_title( '<h4>', '</h4>' );
				?>
            </a>
            <p class="post-card-decription">
				<?php the_excerpt(); ?>
            </p>
            <a href="<?php the_permalink() ?>" class="small-button ghost-button w-button">Read More</a>
        </div>

    </div>
</div>
