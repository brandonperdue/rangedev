<?php defined( 'ABSPATH' ) || exit; ?><!DOCTYPE html><!--  Last Published: Tue Nov 20 2018 19:51:23 GMT+0000 (UTC)  --><html data-wf-page="5b4f4f6a6c24e55b55e09e33" data-wf-site="5ab00568c82e2061c2a05a58"><head>
  <meta charset="utf-8">
  
  
  
  
  
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>css/normalize.css?v=1542837576" rel="stylesheet" type="text/css">
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>css/components.css?v=1542837576" rel="stylesheet" type="text/css">
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>css/range-developments.css?v=1542837576" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Roboto:regular,700"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>images/favicon.png?v=1542837576" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo trailingslashit(get_stylesheet_directory_uri()); ?>images/webclip.png?v=1542837576" rel="apple-touch-icon">
  <meta name="ahrefs-site-verification" content="714c4b625dd53181fcfed919e97e4e4ae294cc8fb251d1cb911b02256a21e297">
  <meta name="google-site-verification" content="7_G9HvmS0Ee9hApkUO-UoLrk6eJx75oDWDroa2R4otE">
  <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
  <!--  Global site tag (gtag.js) - Google Analytics  -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-4118182-47"></script>
  <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-4118182-47');
</script>
  <script src="https://global.localizecdn.com/localize.js"></script>
  <script>!function(a){if(!a.Localize){a.Localize={};for(var e=["translate","untranslate","phrase","initialize","translatePage","setLanguage","getLanguage","detectLanguage","getAvailableLanguages","untranslatePage","bootstrap","prefetch","on","off"],t=0;t<e.length;t++)a.Localize[e[t]]=function(){}}}(window);</script>
  <script>
 Localize.initialize({
  key: 'reyBHeN3Gieh5',
  rememberLanguage: true
 });
</script>
  <script>
window.onload = function(){
 if (Localize.getLanguage() === "ar") {
   document.getElementsByTagName("body")[0].setAttribute("dir", "rtl");
   document.getElementById("Hero").setAttribute("dir", "ltr");
   if (document.getElementById("slide1")) {document.getElementById("slide1").setAttribute("dir", "rtl");}
   if (document.getElementById("slide2")) {document.getElementById("slide2").setAttribute("dir", "rtl");}
   if (document.getElementById("hero-call-text")) {document.getElementById("hero-call-text").setAttribute("dir", "rtl");}
   if (document.getElementById("phone-number-dir")) {document.getElementById("phone-number-dir").setAttribute("dir", "ltr");}
   if (document.getElementById("Testimonials")) {document.getElementById("Testimonials").setAttribute("dir", "ltr");}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").setAttribute("dir", "rtl");}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").setAttribute("dir", "rtl");}
   document.getElementsByTagName("body")[0].style.textAlign = "right";
   document.getElementById("Hero").style.textAlign = "right";
   if (document.getElementById("slide1")) {document.getElementById("slide1").style.textAlign = "right";}
   if (document.getElementById("slide2")) {document.getElementById("slide2").style.textAlign = "right";}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").style.textAlign = "right";}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").style.textAlign = "right";}
   if ($('.vertical-header-wrapper')) {$('.vertical-header-wrapper').css('right', '0'); $('.vertical-header').css('left', '124px');}
   if (document.getElementById("wistia-bqquvi10jm-1_popover")) {document.getElementById("wistia-bqquvi10jm-1_popover").style.right = "-99999px";document.getElementById("wistia-bqquvi10jm-1_popover").style.left = "auto";}
   if (document.getElementById("wistia-bqquvi10jm-1_popover_popover_close_button")) {document.getElementById("wistia-bqquvi10jm-1_popover_popover_close_button").style.right = "-99999px";document.getElementById("wistia-bqquvi10jm-1_popover").style.left = "auto";}
 }
}
Localize.on("setLanguage", function(data) {
 if (data.to === "ar") {
   document.getElementsByTagName("body")[0].setAttribute("dir", "rtl");
   document.getElementById("Hero").setAttribute("dir", "ltr");
   if (document.getElementById("slide1")) {document.getElementById("slide1").setAttribute("dir", "rtl");}
   if (document.getElementById("slide2")) {document.getElementById("slide2").setAttribute("dir", "rtl");}
   if (document.getElementById("hero-call-text")) {document.getElementById("hero-call-text").setAttribute("dir", "rtl");}
   if (document.getElementById("phone-number-dir")) {document.getElementById("phone-number-dir").setAttribute("dir", "ltr");}
   if (document.getElementById("Testimonials")) {document.getElementById("Testimonials").setAttribute("dir", "ltr");}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").setAttribute("dir", "rtl");}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").setAttribute("dir", "rtl");}
   document.getElementsByTagName("body")[0].style.textAlign = "right";
   document.getElementById("Hero").style.textAlign = "right";
   if (document.getElementById("slide1")) {document.getElementById("slide1").style.textAlign = "right";}
   if (document.getElementById("slide2")) {document.getElementById("slide2").style.textAlign = "right";}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").style.textAlign = "right";}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").style.textAlign = "right";}
 }
 if (data.to != "ar") {
   document.getElementsByTagName("body")[0].setAttribute("dir", "ltr");
   if (document.getElementById("slide1")) {document.getElementById("slide1").setAttribute("dir", "ltr");}
   if (document.getElementById("slide2")) {document.getElementById("slide2").setAttribute("dir", "ltr");}
   if (document.getElementById("hero-call-text")) {document.getElementById("hero-call-text").setAttribute("dir", "ltr");}
   if (document.getElementById("phone-number-dir")) {document.getElementById("phone-number-dir").setAttribute("dir", "ltr");}
   if (document.getElementById("Testimonials")) {document.getElementById("Testimonials").setAttribute("dir", "ltr");}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").setAttribute("dir", "ltr");}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").setAttribute("dir", "ltr");}
   document.getElementsByTagName("body")[0].style.textAlign = "left";
   document.getElementById("Hero").style.textAlign = "left";
   if (document.getElementById("slide1")) {document.getElementById("slide1").style.textAlign = "left";}
   if (document.getElementById("slide2")) {document.getElementById("slide2").style.textAlign = "left";}
   if (document.getElementById("test-slide-1")) {document.getElementById("test-slide-1").style.textAlign = "left";}
   if (document.getElementById("test-slide-2")) {document.getElementById("test-slide-2").style.textAlign = "left";}
 }
})
</script>
<?php wp_enqueue_script("jquery"); wp_head(); ?></head>
<body class="body <?php echo " ".join( ' ', get_body_class() ); ?>" udesly-page-name="style-guide"><?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div data-ix="preloader" class="preloader"></div>
  <div data-ix="page-load" class="page-wrapper">
    <div id="Hero" class="image-section style-guide-section">
      <div class="wrapper w-container">
        <div class="page-hero"><a href="<?php echo get_site_url(); ?>" class="page-logo w-nav-brand"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-719241259"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_-719241259','image-src'); ?>" width="150" alt="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_-719241259','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_-719241259','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></a>
          <div class="blog-hero-button">
            <div class="blog-hero-button-info">See the basic styles below</div><a href="<?php echo get_site_url(); ?>" class="small-button white-button w-button">Back Home</a></div>
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1499611566"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_1499611566','text'); ?></udesly-fe>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="section-header-wrapper">
        <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1238521704"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_-1238521704','text'); ?></udesly-fe>
      </div>
      <div class="wrapper w-container">
        <div class="button-styles">
          <div class="button-style-wrapper"><a href="#" class="button w-button">Default Button</a></div>
          <div class="button-style-wrapper"><a href="#" class="button ghost-button w-button">Ghost Button</a></div>
          <div class="button-style-wrapper dark-bg"><a href="#" class="button white-button w-button">White Button</a></div>
          <div class="button-style-wrapper dark-bg"><a href="#" class="button ghost-white-button w-button">Ghost Button</a></div>
          <div class="button-style-wrapper"><a href="#" class="small-button w-button">Default Button (S)</a></div>
          <div class="button-style-wrapper"><a href="#" class="small-button ghost-button w-button">Ghost Button (S)</a></div>
          <div class="button-style-wrapper dark-bg"><a href="#" class="small-button white-button w-button">White&#xA0;Button (S)</a></div>
          <div class="button-style-wrapper dark-bg"><a href="#" class="small-button ghost-white-button w-button">Ghost Button (S)</a></div>
        </div>
        <div class="style-block typography-styles text-center">
          <div class="section-header-wrapper">
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1140990008"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_1140990008','text'); ?></udesly-fe>
          </div>
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_598551891"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_598551891','text'); ?></udesly-fe>
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1035091680"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_-1035091680','text'); ?></udesly-fe>
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1170932171"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_-1170932171','text'); ?></udesly-fe>
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1821029243"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_1821029243','text'); ?></udesly-fe>
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1960482924"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_1960482924','text'); ?></udesly-fe>
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1382005042"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_-1382005042','text'); ?></udesly-fe>
        </div>
        <div class="style-block typography-styles">
          <div class="section-header-wrapper">
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-766848860"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_-766848860','text'); ?></udesly-fe>
          </div>
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-649216346"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_-649216346','text'); ?></udesly-fe>
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_872866863"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_872866863','text'); ?></udesly-fe>
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_13652830"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_13652830','text'); ?></udesly-fe>
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_126564934"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_126564934','text'); ?></udesly-fe>
        </div>
        <div class="style-block">
          <div class="section-header-wrapper">
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1612981619"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_-1612981619','text'); ?></udesly-fe>
          </div>
          <div class="post-content w-richtext">
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-2084703987"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_-2084703987','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1743638689"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_1743638689','text'); ?></udesly-fe>
            <figure class="w-richtext-align-fullwidth w-richtext-figure-type-image">
              <div><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_760955621"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_760955621','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_760955621','image-alt'); ?>" srcset="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_760955621','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></div>
            </figure>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_788434074"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_788434074','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_274603053"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_274603053','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1225330904"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_1225330904','text'); ?></udesly-fe>
            <blockquote>The rich text element allows you to create and format headings, paragraphs, blockquotes, images, and video all in one place</blockquote>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-961904436"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_-961904436','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1372321140"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_1372321140','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-231776588"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_-231776588','text'); ?></udesly-fe>
            <ul>
              <li>List item number one</li>
              <li>Second list item</li>
              <li>And this is the third item</li>
            </ul>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1550747755"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_1550747755','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1124116311"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_1124116311','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_788434074"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_788434074','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1795967924"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_1795967924','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-231776588"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_-231776588','text'); ?></udesly-fe>
            <ol>
              <li>List item number one</li>
              <li>Second list item</li>
              <li>And this is the third item</li>
            </ol>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1550747755"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_1550747755','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1568017047"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_1568017047','text'); ?></udesly-fe>
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_788434074"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_788434074','text'); ?></udesly-fe>
          </div>
        </div>
      </div>
    </div>
    <div class="section instagram-section">
      <div class="instagram">
        <div class="instagram-photos">
          <a data-ix="instagram-photo" href="#" target="_blank" class="instagram-photo photo-1 w-inline-block">
            <div class="photo-fade"></div>
            <div class="photo-hover"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_1208653008"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_1208653008','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_1208653008','image-alt'); ?>" class="photo-plus-icon" srcset="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_1208653008','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></div>
          </a>
          <a data-ix="instagram-photo" href="#" target="_blank" class="instagram-photo photo-2 w-inline-block">
            <div class="photo-hover">
              <div class="photo-hover"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_1208653008"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_1208653008','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_1208653008','image-alt'); ?>" class="photo-plus-icon" srcset="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_1208653008','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></div>
            </div>
          </a>
          <a data-ix="instagram-photo" href="#" target="_blank" class="instagram-photo photo-3 w-inline-block">
            <div class="photo-hover"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_1208653008"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_1208653008','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_1208653008','image-alt'); ?>" class="photo-plus-icon" srcset="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_1208653008','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></div>
          </a>
          <a data-ix="instagram-photo" href="#" target="_blank" class="instagram-photo photo-5 w-inline-block">
            <div class="photo-hover"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_1208653008"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_1208653008','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_1208653008','image-alt'); ?>" class="photo-plus-icon" srcset="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_1208653008','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></div>
          </a>
          <a data-ix="instagram-photo" href="#" class="instagram-photo photo-4 w-inline-block">
            <div class="photo-hover"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_1208653008"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_1208653008','image-src'); ?>" alt="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_1208653008','image-alt'); ?>" class="photo-plus-icon" srcset="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_1208653008','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe></div>
          </a>
        </div>
        <div class="instagram-info-wrapper">
          <div class="instagram-info">
            <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_202938515"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_202938515','text'); ?></udesly-fe><a target="_blank" href="<?php echo udesly_get_permalink_by_slug('about-range-developments'); ?>" class="button instagram-button w-button">Feel Good</a></div>
        </div>
      </div>
    </div>
    <div id="Features" class="section no-bottom-padding">
      <div class="vertical-header-wrapper">
        <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_-1986908046"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_-1986908046','text'); ?></udesly-fe>
      </div>
      <div class="wrapper w-container">
        <div class="section-header-wrapper">
          <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1765451729"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_1765451729','text'); ?></udesly-fe>
        </div>
        <div class="features">
          <div data-ix="fade-up-1" class="feature"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_-290626348"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_-290626348','image-src'); ?>" width="56" alt="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_-290626348','image-alt'); ?>" class="feature-icon" srcset="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_-290626348','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe>
            <div class="feature-content">
              <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_420005488"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_420005488','text'); ?></udesly-fe>
              <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1774961578"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_1774961578','text'); ?></udesly-fe>
            </div>
          </div>
          <div data-ix="fade-up-2" class="feature"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_1885007152"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_1885007152','image-src'); ?>" width="56" alt="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_1885007152','image-alt'); ?>" class="feature-icon" srcset="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_1885007152','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe>
            <div class="feature-content">
              <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1387729957"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_1387729957','text'); ?></udesly-fe>
              <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1774961578"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_1774961578','text'); ?></udesly-fe>
            </div>
          </div>
          <div data-ix="fade-up-3" class="feature"><udesly-fe class="udesly-frontend-editor-image" udesly-data-name="image_634481969"><div style="display: none;" udesly-data-editable=""></div><img src="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_634481969','image-src'); ?>" width="56" alt="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_634481969','image-alt'); ?>" class="feature-icon" srcset="<?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_image_634481969','image-srcset'); ?>"><div media-type="image" class="udesly-wp-media-btn" style="display:none;"></div></udesly-fe>
            <div class="feature-content">
              <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1413538273"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_1413538273','text'); ?></udesly-fe>
              <udesly-fe class="udesly-frontend-editor-text" udesly-data-editable="" udesly-data-name="text_1774961578"><?php echo udesly_get_frontend_editor_content('style-guide_udesly_frontend_editor_text_1774961578','text'); ?></udesly-fe>
            </div>
          </div>
        </div>
      </div>
    </div>
	<?php get_footer(); ?>
<?php wp_footer(); ?><?php endwhile; endif; ?></body></html>