<?php
/**
 * Created by PhpStorm.
 * User: umberto
 * Date: 12/04/2018
 * Time: 11:44
 */

namespace UdyWfToWp\Plugins\FrontendEditor;

use UdyWfToWp\Core\Udy_Wf_To_Wp;
use UdyWfToWp\Plugins\ContentManager\Settings\Settings_Manager;
use UdyWfToWp\Utils\Utils;

class FrontendEditor_Assets{

	public static $assets_folder_url = UDY_WF_TO_WP_PLUGIN_DIRECTORY_URL . 'includes/plugins/frontendeditor/assets/';

	public static function enqueue_styles(){
		//enable the frontend editor for admin
		if (FrontendEditor_Configuration::current_user_can_use_frontend_editor()) {
			wp_enqueue_style( FrontendEditor_Configuration::$plugin_name . '-content-tools-css', self::$assets_folder_url . 'css/content-tools.min.css', array(), Utils::getPluginVersion(), 'all' );
			wp_enqueue_style( FrontendEditor_Configuration::$plugin_name, self::$assets_folder_url . 'css/udesly-frontend-editor-public.css', array( FrontendEditor_Configuration::$plugin_name . '-content-tools-css' ), Utils::getPluginVersion(), 'all' );
		}
	}

	public static function enqueue_scripts(){
		//enable the frontend editor for admin
		if (FrontendEditor_Configuration::current_user_can_use_frontend_editor()) {
			wp_enqueue_script( FrontendEditor_Configuration::$plugin_name . '-content-tools-js', self::$assets_folder_url . 'js/content-tools.js', array(), Utils::getPluginVersion(), true );
			wp_enqueue_script( FrontendEditor_Configuration::$plugin_name, self::$assets_folder_url . 'js/udesly-frontend-editor-public.js', array(
				'jquery',
				FrontendEditor_Configuration::$plugin_name . '-content-tools-js'
			), Utils::getPluginVersion(), true );
			wp_localize_script( FrontendEditor_Configuration::$plugin_name, 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
			wp_enqueue_media();
		}else{
			wp_enqueue_script( FrontendEditor_Configuration::$plugin_name . '-disabled-js', self::$assets_folder_url . 'js/udesly-frontend-editor-disabled.js', array(), Utils::getPluginVersion(), true );
		}
	}

	public static function enqueue_admin_styles(){
		//enable the frontend editor for admin
		if (FrontendEditor_Configuration::current_user_can_use_frontend_editor()) {
			wp_enqueue_style( FrontendEditor_Configuration::$plugin_name . '-admin', self::$assets_folder_url . 'css/udesly-frontend-editor-admin.css', array(), Utils::getPluginVersion(), 'all' );
		}
	}


	public static function enqueue_admin_scripts(){
		//enable the frontend editor for admin
		if (FrontendEditor_Configuration::current_user_can_use_frontend_editor()) {
			wp_enqueue_script( FrontendEditor_Configuration::$plugin_name . '-admin', self::$assets_folder_url . 'js/udesly-frontend-editor-admin.js', array(
				'jquery'
			), Utils::getPluginVersion(), true );
			wp_localize_script( FrontendEditor_Configuration::$plugin_name . '-admin', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		}
	}

	public function delete_frontend_editor_page_elements(){
		if( isset($_POST['post_id']) && isset($_POST['action']) && $_POST['action'] == 'delete_frontend_editor_page_elements' && FrontendEditor_Configuration::current_user_can_use_frontend_editor()) {
			$post = get_post($_POST['post_id']);
			global $wpdb;
			echo $wpdb->query('DELETE FROM ' . $wpdb->posts . ' WHERE `post_title` LIKE \'%' . $post->post_name . '_udesly_frontend_editor_%\' AND `post_type` LIKE \'udesly-fe\'');
		}
		wp_die();
	}

	public function save_content_editable() {
		//enable the frontend editor for admin
		if ( FrontendEditor_Configuration::current_user_can_use_frontend_editor() ) {

			$contents_block = $_POST['payload'];
			$images = $_POST['images'];
			$videos = $_POST['videos'];
			$bg_images = $_POST['bg_images'];
			$iframes = $_POST['iframes'];

			foreach ( $contents_block as $post_name => $block ) {
				// $id = post ID
				// $block = contenuto HTML

				$post = get_page_by_title(sanitize_key($post_name), 'OBJECT', 'udesly-fe');

				if(is_null($post))
					continue;

				$post_id          = $post->ID;
				$string_to_update = array(
					'ID'           => $post_id,
					'post_content' => $block,
				);
				$contents_block_result = wp_update_post( $string_to_update );

			}

			foreach ( $images as $post_name => $image_id ){

				$post = get_page_by_title(sanitize_key($post_name), 'OBJECT', 'udesly-fe');

				if(is_null($post))
					continue;

				$post_content = json_encode(array(
					'src' => wp_get_attachment_image_src(sanitize_key($image_id), 'full')[0],
					'srcset' => wp_get_attachment_image_srcset(sanitize_key($image_id)),
					'alt' => trim( strip_tags( get_post_meta( sanitize_key($image_id), '_wp_attachment_image_alt', true ) ) )
				));

				$string_to_update = array(
					'ID'           => $post->ID,
					'post_content' => wp_kses_post($post_content),
				);
				$images_result = wp_update_post( $string_to_update );
			}

			foreach ( $bg_images as $post_name => $image_id ){

				$post = get_page_by_title(sanitize_key($post_name), 'OBJECT', 'udesly-fe');

				if(is_null($post))
					continue;

				$post_content = wp_get_attachment_image_src(sanitize_key($image_id), 'full')[0];

				$string_to_update = array(
					'ID'           => $post->ID,
					'post_content' => wp_kses_post($post_content),
				);
				$bg_images_result = wp_update_post( $string_to_update );
			}

			foreach ( $videos as $post_name => $video_id ){

				$post = get_page_by_title(sanitize_key($post_name), 'OBJECT', 'udesly-fe');

				if(is_null($post))
					continue;

				$media_element_url = wp_get_attachment_url(sanitize_key($video_id));

				$post_content = json_encode(array(
					'videos' => $media_element_url
				));

				$string_to_update = array(
					'ID'           => $post->ID,
					'post_content' => wp_kses_post($post_content),
				);
				$videos_result = wp_update_post( $string_to_update );
			}

			foreach ( $iframes as $post_name => $iframe_src ){

				$post = get_page_by_title(sanitize_key($post_name), 'OBJECT', 'udesly-fe');

				if(is_null($post))
					continue;

				$string_to_update = array(
					'ID'           => $post->ID,
					'post_content' => wp_kses_post($iframe_src)
				);
				$iframes_result = wp_update_post( $string_to_update );
			}

			if($contents_block_result != 0 || $images_result != 0 || $videos_result != 0 || $bg_images_result != 0 || $iframes_result != 0){

				if(isset($contents_block[0])){
					$page_name = explode('_udesly_frontend_editor', $contents_block[0]);

					if(isset($page_name[0])) {
						$post = get_page_by_title( $page_name[0]);

						if($post)
							FrontendEditor_Configuration::sync_page_fe_editor($post->ID,$post->post_name);

					}
				}

				echo true;
			}else{
				echo false;
			}

			wp_die();
		}
	}
}