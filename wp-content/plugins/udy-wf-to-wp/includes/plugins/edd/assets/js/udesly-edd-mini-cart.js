(function ($) {
    'use strict';

    var regex = /<div style="display: none;" id="udesly-edd-mini-cart-items">(.*)<\/div>/g;
    var cart_items = [];

    $(document.body).on('edd_cart_item_added', function (event, res) {
       var item =  regex.exec(res.cart_item);

       var cart_total = res.total;
       var cart_quantity = parseInt(res.cart_quantity);

        if(item[1]){
            cart_items = JSON.parse(item[1]);
        }

        refreshCartActions(cart_total, cart_quantity);
        refreshCartItems(cart_items);
    });

    function refreshCartActions(cart_total, cart_quantity) {
        if (cart_quantity == 0) {
            $('[udesly-edd-el="mini-cart-actions"]').hide();
            $('[udesly-edd-el="no-items-in-cart"]').show();
        } else {
            $('[udesly-edd-el="mini-cart-actions"]').show();
            $('[udesly-edd-el="mini-cart-actions"] [udesly-edd-el="total"]').html(cart_total);
            $('[udesly-edd-el="no-items-in-cart"]').hide();
        }
        $('.edd-cart-quantity').html(cart_quantity);
    }

    function refreshCartItems(cart_items) {
        var dummyCartItem = $('.udesly-edd-mini-cart-dummy-item');
        var dummyHTML = dummyCartItem.wrap('<div></div>').parent().html();

        if(!dummyHTML){
            return;
        }

        var cartItemsContainer = dummyCartItem.closest('.w-dyn-items');
        var result = '';
        cart_items.forEach( function (element) {
            var currentHTML = dummyHTML.replace(/{{title}}/, element.title);
            currentHTML = currentHTML.replace(/{{permalink}}/, element.permalink);
            currentHTML = currentHTML.replace(/{{quantity}}/, element.quantity);
            currentHTML = currentHTML.replace(/{{remove_url}}/, getURLParameters(element.remove_url));
            currentHTML = currentHTML.replace(/{{remove_url_nonce}}/, element.remove_url_nonce);
            currentHTML = currentHTML.replace(/{{remove_url_item_id}}/, element.remove_url_item_id);
            currentHTML = currentHTML.replace(/{{remove_url_cart_item_id}}/, element.remove_url_cart_item_id);
            currentHTML = currentHTML.replace(/{{image}}/, element.image);
            currentHTML = currentHTML.replace(/udesly-edd-mini-cart-dummy-item/, element.class);
            result += currentHTML;
        });
       cartItemsContainer.html(result);
    }

    function getURLParameters(sPageURL){
        return sPageURL.slice(sPageURL.indexOf('?'));
    }
})(jQuery);