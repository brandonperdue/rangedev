<?php
/**
 * Created by PhpStorm.
 * User: umberto
 * Date: 16/04/2018
 * Time: 11:01
 */

function udesly_get_frontend_editor_content($name, $type){

	$post = get_page_by_title( $name, 'OBJECT', 'udesly-fe' );

	if(is_null($post) || is_wp_error($post))
		return;

	switch ($type) {
		case 'text':
			return $post->post_content;
			break;

		case 'image-src':
			$post_content = json_decode($post->post_content);

			if( strpos($post_content->src,'http') !== false){
				return $post_content->src;
			}

			$image_dir = trailingslashit(get_stylesheet_directory_uri());

			return $image_dir.$post_content->src;
			break;

		case 'image-alt':
			$post_content = json_decode($post->post_content);

			if(isset($post_content->alt)){
				return $post_content->alt;
			}

			return '';
			break;

		case 'image-srcset':
			$post_content = json_decode($post->post_content);
			$image_dir = trailingslashit(get_stylesheet_directory_uri());

			$srcset_string = '';


			if(is_string($post_content->srcset))
				return $post_content->srcset;

			$srcset_size = count($post_content->srcset);
			for($i = 0; $i < $srcset_size; $i++){
				if($i != $srcset_size-1) {
				    if(!is_null($post_content->srcset[ $i ])) {
                        if (strpos($post_content->srcset[$i], 'http') !== false) {
                            $srcset_string .= $post_content->srcset[$i] . ', ';
                        } else {
                            $srcset_string .= $image_dir . $post_content->srcset[$i] . ', ';
                        }
                    }
				}else{
                    if(!is_null($post_content->srcset[ $i ])) {
                        if (strpos($post_content->srcset[$i], 'http') !== false) {
                            $srcset_string .= $post_content->srcset[$i];
                        } else {
                            $srcset_string .= $image_dir . $post_content->srcset[$i];
                        }
                    }
				}
			}

			return $srcset_string;
			break;

		case 'video':
			$post_content = json_decode($post->post_content);
			$video_dir = trailingslashit(get_stylesheet_directory_uri());

			if(is_string($post_content->videos)){
				$video_src = $post_content->videos;
				if (strpos($video_src,'http') !== false){
					return "<source src='$video_src' data-wf-ignore='true'>";
				}
				else {
					return "<source src='$video_dir$video_src' data-wf-ignore='true'>";
				}

			}


			$html = '';
			foreach ($post_content->videos as $video_src){
				if (strpos($video_src,'http') !== false){
					$html .= "<source src='$video_src' data-wf-ignore='true'>";
				}else {
					$html .= "<source src='$video_dir$video_src' data-wf-ignore='true'>";
				}
			}

			return $html;

			break;

		case 'bg-image':
			$post_content = $post->post_content;

			if($post_content == 'udesly-no-content')
				return '';

			if( strpos($post_content,'http') !== false){
				return 'background-image: url('.$post_content.')';
			}

			$image_dir = trailingslashit(get_stylesheet_directory_uri());
			return 'background-image: url('.$image_dir.$post_content.')';

			break;

		case 'iframe':
			return $post->post_content;

			break;
	}

}